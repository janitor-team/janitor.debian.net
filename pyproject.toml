[project]
name = "debian-janitor"
authors = [{name = "Jelmer Vernooij", email = "jelmer@jelmer.uk"}]
description = "Automated improvements for Debian package repositories"
requires-python = ">=3.10"
dependencies = [
    "janitor[debian,gcp]@git+https://github.com/jelmer/janitor",
    "pygments",
    "jinja2",
    "python-mimeparse",
    "ruamel.yaml",
    "python-debianbts",
    "lintian-brush@git+https://salsa.debian.org/jelmer/lintian-brush",
    # Pull in https://github.com/jelmer/aiohttp-openmetrics/pull/1
    "aiohttp-openmetrics@git+https://github.com/jelmer/aiohttp-openmetrics",
    "aiohttp-jinja2",
    "python_apt@git+https://salsa.debian.org/apt-team/python-apt.git",
    "brz-debian[udd,launchpad]@git+https://github.com/breezy-team/breezy-debian",
    "debmutate@git+https://salsa.debian.org/jelmer/debmutate",
    "pysimplesoap[httplib2]@git+https://github.com/jelmer/pysimplesoap",
    "httplib2>=0.7.8",
    "uvloop",
]
dynamic = ["version"]

[project.readme]
file = "README.md"
content-type = "text/markdown"

[project.urls]
Homepage = "https://janitor.debian.net/"
Repository = "https://salsa.debian.org/jelmer/janitor.debian.net/"

[project.optional-dependencies]
dev = [
    "flake8>=5.0.4",
    "flake8-tidy-imports",
    "djlint",
    "mock",
    "mypy",
    "yamllint",
    "pytest",
    "pytest-cov",
    "pytest-aiohttp",
    "fakeredis",
    "types-PyYAML",
    "types-redis",
    "types-psycopg2",
    "asyncpg-engine>=0.3.1",
    "testing.postgresql"
]

[build-system]
requires = ["setuptools", "setuptools_protobuf[mypy]>=0.1.3", "setuptools-rust"]
build-backend = "setuptools.build_meta"

[tool.mypy]
warn_redundant_casts = true
warn_unused_configs = true
check_untyped_defs = true

[[tool.mypy.overrides]]
module = [
    "asyncpg.*",
    "aiohttp_apispec.*",
    "upstream_ontologist.*",
    "lintian_brush.*",
    "distro_info.*",
    "gpg.*",
    "aiohttp_openmetrics.*",
    "mimeparse.*",
    "ruamel.*",
    "silver_platter.*",
    "ognibuild.*",
    "google.cloud.logging.*",
    "aiohttp_debugtoolbar.*",
    "buildlog_consultant.*",
    "asyncpg_engine.*",
    "testing.*",
]
ignore_missing_imports = true

[tool.setuptools.packages.find]
where = ["."]

[tool.setuptools.package-data]
debian_janitor = ["py.typed", "debian-janitor.sql"]
"debian_janitor.site" = [
    "templates/*.html",
    "templates/*/*.html",
    "_static/*.css",
    "_static/*.js",
    "_static/*.png",
]

[tool.pytest.ini_options]
asyncio_mode = "auto"
addopts = [
    "--cov=debian_janitor",
    "--cov-report=html"
]

[tool.codespell]
ignore-words = ".codespell-ignore-words"
