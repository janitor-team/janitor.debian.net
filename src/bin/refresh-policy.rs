use clap::{Arg, Command};
use debian_janitor::policy::{sync_publish_policies, validate_policy, PolicyApplier};
use janitor::config::read_file;
use janitor::state;

#[tokio::main]
async fn main() {
    let matches = Command::new("refresh-policy")
        .arg(
            Arg::new("config")
                .long("config")
                .value_name("FILE")
                .default_value("janitor.conf")
                .help("Path to configuration."),
        )
        .arg(
            Arg::new("policy")
                .long("policy")
                .value_name("FILE")
                .default_value("policy.conf")
                .help("Path to policy configuration."),
        )
        .arg(
            Arg::new("debug")
                .long("debug")
                .action(clap::ArgAction::SetTrue)
                .help("Enable debug mode."),
        )
        .arg(
            Arg::new("publisher-url")
                .long("publisher-url")
                .value_name("URL")
                .help("Publisher URL."),
        )
        .arg(
            Arg::new("validate")
                .long("validate")
                .action(clap::ArgAction::SetTrue)
                .help("Perform policy validation."),
        )
        .arg(
            Arg::new("gcp-logging")
                .long("gcp-logging")
                .action(clap::ArgAction::SetTrue)
                .help("Use Google Cloud Logging."),
        )
        .arg(
            Arg::new("package-overrides")
                .long("package-overrides")
                .value_name("FILE")
                .help("Package overrides."),
        )
        .arg(
            Arg::new("package")
                .value_name("PACKAGE")
                .help("Package name."),
        )
        .get_matches();

    let config_path: &String = matches.get_one("config").unwrap();
    let policy_path: &String = matches.get_one("policy").unwrap();
    let debug_mode = matches.get_flag("debug");
    let publisher_url: Option<&String> = matches.get_one("publisher-url");
    let gcp_logging = matches.get_flag("gcp-logging");
    let validate = matches.get_flag("validate");
    let package_overrides: Option<&String> = matches.get_one("package-overrides");
    let package: Option<&String> = matches.get_one("package");

    if gcp_logging {
        stackdriver_logger::init_with_cargo!("../../Cargo.toml");
    } else if debug_mode {
        env_logger::init();
    } else {
        env_logger::builder()
            .filter(None, log::LevelFilter::Info)
            .init();
    }

    let config = read_file(std::path::Path::new(config_path)).unwrap();

    if validate {
        let policy =
            debian_janitor::policy::read_policy(std::path::Path::new(policy_path)).unwrap();

        validate_policy(&config, &policy).unwrap();
    } else {
        let db = state::create_pool(&config).await.unwrap();

        let mut policy_applier = PolicyApplier::from_paths(
            std::path::Path::new(policy_path),
            package_overrides.as_ref().map(std::path::Path::new),
        )
        .unwrap();
        policy_applier.load_release_stages().await.unwrap();

        let mut conn = db.acquire().await.unwrap();
        let (num_updated, _num_deleted) = sync_publish_policies(
            &mut conn,
            &policy_applier,
            &url::Url::parse(publisher_url.as_ref().unwrap()).unwrap(),
            package.as_ref().map(|p| p.as_str()),
        )
        .await
        .unwrap();

        log::info!("Updated policy for {} packages.", num_updated);
    }
}
