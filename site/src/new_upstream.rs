use crate::AppState;
use axum::response::IntoResponse;
use axum::Router;
use std::sync::Arc;

pub fn fresh_releases_routes(state: Arc<AppState>) -> Router<Arc<AppState>> {
    Router::new()
}

pub fn fresh_snapshots_routes(state: Arc<AppState>) -> Router<Arc<AppState>> {
    Router::new()
}

pub async fn fresh_builds() -> impl IntoResponse {
    todo!()
}
