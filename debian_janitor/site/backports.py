#!/usr/bin/python3

import asyncpg
from aiohttp import web
from debian.changelog import Version
from janitor.config import get_campaign_config
from janitor.site.common import render_template_for_request
from yarl import URL

from ..udd import DEFAULT_UDD_URL


async def get_published_packages(conn: asyncpg.Connection, suite, other_suites):
    versions: dict[str, dict[str, Version]] = {}
    for row in await conn.fetch(
        """
select distinct source, version, distribution from debian_build
where distribution = $1
ORDER BY version DESC
""",
        suite,
    ):
        versions.setdefault(row["source"], {})[row["distribution"]] = row["version"]

    # TODO(jelmer): Fetch this into our database
    udd = await asyncpg.connect(
        DEFAULT_UDD_URL,
    )
    await udd.set_type_codec("debversion", format="text", encoder=str, decoder=Version)
    for row in await udd.fetch(
        "SELECT release, source, version FROM sources "
        "WHERE release = ANY($1::text[])",
        other_suites,
    ):
        versions.setdefault(row["source"], {})[row["release"]] = row["version"]

    ret = []
    for name, vs in sorted(versions.items()):
        if suite in vs:
            ret.append(
                (
                    name,
                    vs.get(suite),
                )
                + tuple([vs.get(s) for s in other_suites])
            )

    return ret


async def handle_apt_repo(target_release, request):
    suite = target_release + "-backports"

    async with request.app.database.acquire() as conn:
        vs = {
            "packages": await get_published_packages(
                conn, "auto-" + suite, [target_release, "sid"]
            ),
            "suite": suite,
            "campaign": suite,
            "URL": URL,
            "Version": Version,
            "source_release": "sid",
            "target_release": target_release,
            "campaign_config": get_campaign_config(request.app["config"], suite),
        }
    text = await render_template_for_request("backports/start.html", request, vs)
    return web.Response(
        content_type="text/html",
        text=text,
        headers={"Cache-Control": "max-age=60", "Vary": "Cookie"},
    )


def register_backports_endpoints(router, target_release):
    router.add_get(
        "/%s-backports/" % target_release,
        lambda r: handle_apt_repo(target_release, r),
        name="%s-backports-start" % target_release,
    )
