use clap::Parser;

use sqlx::postgres::{PgPool, PgPoolOptions};
use sqlx::Arguments;
use std::error::Error;

const DEFAULT_VALUE_ORPHAN: i32 = 60;

#[derive(Parser, Debug)]
struct Args {
    /// Enable debug mode.
    #[arg(long, default_value_t = false)]
    debug: bool,

    /// Use Google cloud logging.
    #[arg(long, default_value_t = false)]
    gcp_logging: bool,

    #[arg(short, long, value_name = "FILE")]
    config: Option<std::path::PathBuf>,

    #[arg(long, value_name = "URL", default_value_t = debian_janitor::udd::default_udd_url())]
    udd_url: String,

    packages: Option<Vec<String>>,
}

#[derive(sqlx::FromRow, Debug)]
struct Orphan {
    source: String,
    age: sqlx::postgres::types::PgInterval,
    bug: Option<i32>,
}

async fn iter_orphan_candidates(
    pool: &PgPool,
    release: &str,
    packages: Option<Vec<&str>>,
) -> Result<(), Box<dyn Error>> {
    let mut query = r#"
SELECT DISTINCT ON (sources.source) sources.source AS source, now() - orphaned_time AS age, bug
FROM sources
JOIN orphaned_packages ON orphaned_packages.source = sources.source
WHERE sources.vcs_url != '' AND sources.release = $1 AND
orphaned_packages.type in ('O') AND
(sources.uploaders != '' OR
sources.maintainer != 'Debian QA Group <packages@qa.debian.org>')
"#
    .to_string();

    let mut args = sqlx::postgres::PgArguments::default();
    args.add(release);

    if let Some(packages) = packages {
        query.push_str(" AND sources.source = ANY($2::text[])");
        args.add(packages);
    }

    let rows: Vec<Orphan> = sqlx::query_as_with(&query, args).fetch_all(pool).await?;

    for row in rows {
        let candidate = debian_janitor::candidates::Candidate {
            distribution: Some(release.to_string()),
            package: Some(row.source),
            campaign: Some("orphan".to_string()),
            context: row.bug.map(|b| b.to_string()),
            value: Some(DEFAULT_VALUE_ORPHAN),
            ..Default::default()
        };

        debian_janitor::candidates::print_candidate_as_textpb(candidate);
    }

    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let args = Args::parse();

    // TODO(jelmer): Add common function for initializing logging
    if args.gcp_logging {
        stackdriver_logger::init_with_cargo!("../../Cargo.toml");
    } else if args.debug {
        env_logger::init();
    } else {
        env_logger::builder()
            .filter(None, log::LevelFilter::Info)
            .init();
    }

    // TODO(jelmer): Add common function for connecting to udd
    let pool = PgPoolOptions::new()
        .max_connections(5)
        .connect(args.udd_url.as_str())
        .await?;

    iter_orphan_candidates(
        &pool,
        "sid",
        args.packages
            .as_ref()
            .map(|ps| ps.iter().map(|s| s.as_str()).collect()),
    )
    .await?;

    Ok(())
}
