use async_stream::try_stream;
use futures_core::stream::Stream;
use log::debug;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::time::Duration;

use reqwest::Client;

pub fn load_from_repology() -> impl Stream<Item = Result<(String, Vec<Project>), reqwest::Error>> {
    try_stream! {
        let mut last_project: Option<String> = None;
        let client = Client::new();
        loop {
            let url = format!(
                "https://repology.org/api/v1/projects/{}",
                last_project.as_deref().unwrap_or("")
            );
            debug!("Retrieving {}", url);
            let response =
                client
                .get(&url)
                .header("Accept", "application/json")
                .send()
                .await?;

            let data: HashMap<String, Vec<Project>> = response.json().await?;
            let data_len = data.len();

            if data_len == 0 {
                break;
            }

            let last_project_key = data.keys().max().unwrap().to_string();
            for (key, value) in data {
                yield (key, value);
            }

            last_project = Some(format!("{}/", last_project_key));
            tokio::time::sleep(Duration::from_secs(1)).await;
        }
    }
}

#[derive(Debug, Deserialize, Serialize, PartialEq, Eq, sqlx::Type)]
#[sqlx(type_name = "repology_status")]
#[sqlx(rename_all = "lowercase")]
pub enum Status {
    #[serde(rename = "newest")]
    Newest,
    #[serde(rename = "rolling")]
    Rolling,
    #[serde(rename = "unique")]
    Unique,
    #[serde(rename = "outdated")]
    Outdated,
    #[serde(rename = "legacy")]
    Legacy,
    #[serde(rename = "ignored")]
    Ignored,
    #[serde(rename = "untrusted")]
    Untrusted,
    #[serde(rename = "noscheme")]
    Noscheme,
    #[serde(rename = "devel")]
    Devel,
    #[serde(rename = "incorrect")]
    Incorrect,
}

impl ToString for Status {
    fn to_string(&self) -> String {
        serde_json::to_string(&self)
            .expect("Failed to serialize Status")
            .replace('\"', "")
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_status() {
        let status: Status = serde_json::from_str("\"newest\"").unwrap();
        assert_eq!(status, Status::Newest);
        assert_eq!(status.to_string(), "newest");
    }
}

#[derive(Debug, Deserialize)]
pub struct Project {
    pub repo: String,
    pub srcname: Option<String>,
    pub binname: Option<String>,
    pub visiblename: String,
    pub version: String,
    pub maintainers: Option<Vec<String>>,
    pub licenses: Option<Vec<String>>,
    pub summary: Option<String>,
    pub status: Status,
    pub origversion: Option<String>,
}
