use pyo3::prelude::*;

fn determine_browser_url(vcs_type: Option<&str>, url: &str) -> Option<String> {
    pyo3::Python::with_gil(|py| {
        let vcs = py.import_bound("lintian_brush.vcs").unwrap();
        let pyfn = vcs.getattr("determine_browser_url").unwrap();
        pyfn.call1((vcs_type, url)).unwrap().extract().unwrap()
    })
}

pub fn split_vcs_url(vcs_url: &str) -> (String, Option<String>, Option<String>) {
    pyo3::Python::with_gil(|py| {
        let vcs = py.import_bound("debmutate.vcs").unwrap();
        let pyfn = vcs.getattr("split_vcs_url").unwrap();
        pyfn.call1((vcs_url,)).unwrap().extract().unwrap()
    })
}

pub fn unsplit_vcs_url(vcs_url: &str, branch_name: Option<&str>, subpath: Option<&str>) -> String {
    pyo3::Python::with_gil(|py| {
        let vcs = py.import_bound("debmutate.vcs").unwrap();
        let pyfn = vcs.getattr("unsplit_vcs_url").unwrap();
        pyfn.call1((vcs_url, branch_name, subpath))
            .unwrap()
            .extract()
            .unwrap()
    })
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_split_vcs_url() {
        assert_eq!(
            super::split_vcs_url("https://github.com/jelmer/example"),
            ("https://github.com/jelmer/example".to_string(), None, None)
        );
        assert_eq!(
            super::split_vcs_url("https://github.com/jelmer/example [path/to/packaging]"),
            (
                "https://github.com/jelmer/example".to_string(),
                None,
                Some("path/to/packaging".to_string())
            )
        );
        assert_eq!(
            super::split_vcs_url("https://github.com/jelmer/example [path/to/packaging] -b master"),
            (
                "https://github.com/jelmer/example".to_string(),
                Some("master".to_string()),
                Some("path/to/packaging".to_string())
            )
        );
        assert_eq!(
            super::split_vcs_url("https://github.com/jelmer/example -b master [path/to/packaging]"),
            (
                "https://github.com/jelmer/example".to_string(),
                Some("master".to_string()),
                Some("path/to/packaging".to_string())
            )
        );
        assert_eq!(
            super::split_vcs_url("https://github.com/jelmer/example -b master"),
            (
                "https://github.com/jelmer/example".to_string(),
                Some("master".to_string()),
                None
            )
        );
    }

    #[test]
    fn test_unsplit_vcs_url() {
        assert_eq!(
            super::unsplit_vcs_url("https://github.com/jelmer/example", None, None),
            "https://github.com/jelmer/example".to_string()
        );
        assert_eq!(
            super::unsplit_vcs_url(
                "https://github.com/jelmer/example",
                None,
                Some("path/to/packaging")
            ),
            "https://github.com/jelmer/example [path/to/packaging]".to_string()
        );
        assert_eq!(
            super::unsplit_vcs_url(
                "https://github.com/jelmer/example",
                Some("master"),
                Some("path/to/packaging")
            ),
            "https://github.com/jelmer/example -b master [path/to/packaging]".to_string()
        );
        assert_eq!(
            super::unsplit_vcs_url("https://github.com/jelmer/example", Some("master"), None),
            "https://github.com/jelmer/example -b master".to_string()
        );
    }
}
