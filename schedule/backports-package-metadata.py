#!/usr/bin/python3
# Copyright (C) 2018 Jelmer Vernooij <jelmer@jelmer.uk>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

"""Exporting of upstream metadata from UDD."""

import logging
from collections.abc import Iterator
from email.utils import parseaddr
from typing import Any

from debian.changelog import Version

from debian_janitor.package_metadata_pb2 import PackageList
from debian_janitor.udd import DEFAULT_UDD_URL


def extract_uploader_emails(uploaders: str) -> list[str]:
    if not uploaders:
        return []
    ret = []
    for uploader in uploaders.split(","):
        if not uploader:
            continue
        email = parseaddr(uploader)[1]
        if not email:
            continue
        ret.append(email)
    return ret


async def iter_backports_packages(
    udd, source_release: str, target_release: str, packages: list[str] | None = None
):
    args: list[Any] = [source_release, target_release]
    query = """
select distinct on (sources.source) sources.source AS source,
sources.maintainer_email AS maintainer_email,
sources.uploaders AS uploaders,
popcon_src.insts AS insts,
sources.vcs_type AS vcs_type,
(CASE WHEN
 sources.vcs_url <> backports_sources.vcs_url then backports_sources.vcs_url
 else NULL end) AS vcs_url,
(CASE WHEN
 sources.vcs_browser <> backports_sources.vcs_browser then backports_sources.vcs_browser
 else NULL end) AS vcs_browser,
sources.version AS version,
sources.original_maintainer as original_maintainer,
sources.release as release
from sources left join popcon_src on sources.source = popcon_src.source
LEFT JOIN sources backports_sources
ON sources.source = backports_sources.source AND backports_sources.release = $2
where sources.release = $1
"""
    if packages:
        query += " and sources.source = ANY($2::text[])"
        args.append(packages)
    query += (
        " order by sources.source, sources.version desc, backports_sources.version desc"
    )
    for row in await udd.fetch(query, *args):
        yield row


async def iter_removals(udd, packages: list[str] | None = None) -> Iterator:
    query = """\
select name, version from package_removal where 'source' = any(arch_array)
"""
    args = []
    if packages:
        query += " and name = ANY($1::text[])"
        args.append(packages)
    return await udd.fetch(query, *args)


async def main():
    import argparse

    import asyncpg

    parser = argparse.ArgumentParser(prog="candidates")
    parser.add_argument("packages", nargs="*")
    parser.add_argument(
        "--gcp-logging", action="store_true", help="Use Google cloud logging."
    )
    parser.add_argument("--udd-url", type=str, default=DEFAULT_UDD_URL, help="UDD URL")
    parser.add_argument("--source-release", type=str, default="testing")
    parser.add_argument("--target-release", type=str)
    args = parser.parse_args()

    if args.gcp_logging:
        import google.cloud.logging

        client = google.cloud.logging.Client()
        client.get_default_handler()
        client.setup_logging()
    else:
        logging.basicConfig(level=logging.INFO)

    udd = await asyncpg.connect(args.udd_url)

    removals = {}
    for name, version in await iter_removals(udd, packages=args.packages):
        if name not in removals:
            removals[name] = Version(version)
        else:
            removals[name] = max(Version(version), removals[name])

    for name, version in removals.items():
        pl = PackageList()
        removal = pl.removal.add()
        removal.name = name
        removal.version = str(version)
        print(pl)

    async for row in iter_backports_packages(
        udd, args.source_release, args.target_release, args.packages
    ):
        pl = PackageList()
        package = pl.package.add()
        package.name = row["source"]
        if row["maintainer_email"] is None:
            logging.warning("package %s has no maintainer set", row["source"])
        else:
            package.maintainer_email = row["maintainer_email"]
        package.uploader_email.extend(extract_uploader_emails(row["uploaders"]))
        package.in_base = row["original_maintainer"] is not None
        if row["insts"] is not None:
            package.insts = row["insts"]
        if row["vcs_type"]:
            package.vcs_type = row["vcs_type"]
            if row["vcs_url"]:
                package.vcs_url = row["vcs_url"]
            if row["vcs_browser"]:
                package.vcs_browser = row["vcs_browser"]
        package.archive_version = row["version"]
        if row["source"] not in removals:
            package.removed = False
        else:
            package.removed = Version(row["version"]) <= removals[row["source"]]
        print(pl)


if __name__ == "__main__":
    import asyncio

    asyncio.run(main())
