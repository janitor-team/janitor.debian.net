# set ft=textproto
upstream_project {
  name: "erbium"
  vcs_type: "git"
  vcs_url: "https://github.com/isomer/erbium.git"
}
upstream_project {
  name: "bup"
  vcs_type: "git"
  vcs_url: "https://github.com/bup/bup.git"
}

# Bug 985964: ITP: librdata -- C/C++ library to manipulate GNU R data frames.
upstream_project {
  name: "librdata"
  vcs_type: "Git"
  vcs_url: "https://github.com/WizardMac/librdata"
}

# Bug 985961: RFP: elpa-ednc -- Emacs Desktop Notification Center
upstream_project {
  name: "elpa-ednc"
  vcs_type: "Git"
  vcs_url: "https://github.com/sinic/ednc"
}

# Bug 985945: ITP: nthash -- recursive hash function for hashing all possible k-mers in a DNA/RNA sequence
upstream_project {
  name: "nthash"
  vcs_type: "Git"
  vcs_url: "https://github.com/bcgsc/ntHash"
}

# Bug 985939: ITP: wiredpanda -- logic circuits simulator
upstream_project {
  name: "wiredpanda"
  vcs_type: "Git"
  vcs_url: "https://github.com/GIBIS-UNIFESP/wiRedPanda"
}

# Bug 985931: ITP: golang-github-aryann-difflib -- go library for diffing two sequences of text
upstream_project {
  name: "golang-github-aryann-difflib-dev"
  vcs_type: "Git"
  vcs_url: "https://github.com/aryann/difflib"
}

# Bug 985918: ITP: bung -- backup next generation
upstream_project {
  name: "bung"
  vcs_type: "Git"
  vcs_url: "https://redmine.auroville.org.in/projects/bung/files"
}

# Bug 985909: ITP: open62541 -- implementation of OPC UA (IEC 62541)
upstream_project {
  name: "open62541"
  vcs_type: "Git"
  vcs_url: "http://open62541.org"
}

# Bug 985895: ITP: bamclipper -- Remove gene-specific primer sequences from SAM/BAM alignments
upstream_project {
  name: "bamclipper"
  vcs_type: "Git"
  vcs_url: "https://github.com/tommyau/bamclipper"
}

# Bug 985878: ITP: golang-github-mholt-acmez -- Premier ACME client library for Go
upstream_project {
  name: "golang-github-mholt-acmez"
  vcs_type: "Git"
  vcs_url: "https://github.com/mholt/acmez"
}

# Bug 985857: RFP: libjs-bootstrap5 -- HTML, CSS and JS framework
upstream_project {
  name: "libjs-bootstrap5"
  vcs_type: "Git"
  vcs_url: "https://getbootstrap.com/"
}

# Bug 985846: RFP: elpa-org-present -- minimalist presentation tool for Emacs org-mode
upstream_project {
  name: "elpa-org-present"
  vcs_type: "Git"
  vcs_url: "https://github.com/rlister/org-present/"
}

# Bug 985838: ITP: python-leidenalg -- Python3 implementation of the Leiden algorithm in C++
upstream_project {
  name: "python-leidenalg"
  vcs_type: "Git"
  vcs_url: "https://github.com/vtraag/leidenalg"
}

# Bug 985831: ITP: fpyutils -- collection of useful non-standard Python functions
upstream_project {
  name: "fpyutils"
  vcs_type: "Git"
  vcs_url: "https://github.com/frnmst/fpyutils/"
}

# Bug 985824: RFP: elpa-om-to-xml -- convert Emacs org-mode files to XML
upstream_project {
  name: "elpa-om-to-xml"
  vcs_type: "Git"
  vcs_url: "https://github.com/ndw/org-to-xml/"
}

# Bug 985823: RFP: elpa-org-ml -- functional API for org-mode
upstream_project {
  name: "elpa-org-ml"
  vcs_type: "Git"
  vcs_url: "https://github.com/ndwarshuis/org-ml/"
}

upstream_project {
  name: "envoyproxy"
  vcs_type: "Git"
  vcs_url: "https://github.com/envoyproxy/envoy"
}

upstream_project {
  name: "libwandio"
  vcs_type: "Git"
  vcs_url: "https://github.com/wanduow/wandio.git"
}

upstream_project {
  name: "libtrace"
  vcs_type: "Git"
  vcs_url: "https://github.com/LibtraceTeam/libtrace.git"
}
upstream_project {
  name: "glome"
  vcs_type: "Git"
  vcs_url: "https://github.com/google/glome.git"
}
