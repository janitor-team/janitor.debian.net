use clap::Parser;

use debian_janitor::candidates::Candidate;
use debversion::Version;

use sqlx::postgres::PgPool;
use sqlx::Arguments;
use std::error::Error;

const DEFAULT_VALUE_UNCOMMITTED: i32 = 60;
const DEFAULT_VALUE_UNVERSIONED: i32 = 50;
const DEFAULT_VALUE_IMPORT_FROM_ALIOTH: i32 = 80;
const UNCOMMITTED_NMU_BONUS: i32 = 60;
const DEFAULT_VALUE_UNCHANGED: i32 = 1;

#[derive(sqlx::Type)]
#[sqlx(type_name = "text")]
enum VcswatchStatus {
    #[sqlx(rename = "OK")]
    Ok,
    #[sqlx(rename = "NEW")]
    New,
    #[sqlx(rename = "OLD")]
    Old,
    #[sqlx(rename = "UNREL")]
    Unreleased,
    #[sqlx(rename = "COMMITS")]
    Commits,
    #[sqlx(rename = "TODO")]
    Todo,
    #[sqlx(rename = "ERROR")]
    Error,
}

#[derive(sqlx::FromRow)]
struct VcswatchRow {
    package: String,
    package_version: Option<Version>,
    status: Option<VcswatchStatus>,
    error: Option<String>,
    hash: Option<String>,
    url: Option<String>,
    vcs: Option<String>,
}

pub async fn iter_candidates(
    pool: &PgPool,
    release: &str,
    packages: Option<Vec<&str>>,
) -> Result<(), Box<dyn Error>> {
    let mut args: sqlx::postgres::PgArguments = sqlx::postgres::PgArguments::default();

    args.add(&release);

    let mut query = "
        SELECT
            sources.source AS package,
            vcswatch.changelog_version AS package_version,
            vcswatch.status AS status,
            vcswatch.error AS error,
            vcswatch.commit_id AS hash,
            vcswatch.url AS url,
            vcswatch.vcs AS vcs
        FROM
            sources
            LEFT JOIN vcswatch ON vcswatch.source = sources.source
        WHERE
            sources.release = $1
    "
    .to_owned();

    if let Some(packages) = packages {
        args.add(&packages);
        query += " AND sources.source = ANY($2::text[])";
    }

    let rows: Vec<VcswatchRow> = sqlx::query_as_with(&query, args)
        .fetch_all(pool)
        .await
        .expect("Failed to fetch candidates");

    for row in rows {
        let mut candidate = Candidate::default();

        match row.status {
            None => {
                candidate.distribution = Some(release.to_string());
                candidate.package = Some(row.package.clone());
                candidate.campaign = Some("unversioned".to_owned());
                candidate.value = Some(DEFAULT_VALUE_UNVERSIONED);
                candidate
                    .extra_arg
                    .push(format!("--package={}", row.package));
            }
            Some(VcswatchStatus::Old) | Some(VcswatchStatus::Unreleased) => {
                let mut value = DEFAULT_VALUE_UNCOMMITTED;

                if row
                    .package_version
                    .as_ref()
                    .unwrap()
                    .to_string()
                    .contains("nmu")
                {
                    value += UNCOMMITTED_NMU_BONUS;
                }

                candidate.distribution = Some(release.to_string());
                candidate.package = Some(row.package.clone());
                candidate.context = Some(row.package_version.as_ref().unwrap().to_string());
                candidate.campaign = Some("uncommitted".to_string());
                candidate.value = Some(value);
            }
            Some(VcswatchStatus::Ok) => {
                continue;
            }
            Some(VcswatchStatus::Commits) | Some(VcswatchStatus::New) => {
                candidate.distribution = Some(release.to_owned());
                candidate.package = Some(row.package.clone());
                candidate.context = row.hash;
                candidate.campaign = Some("unchanged".to_owned());
                candidate.value = Some(DEFAULT_VALUE_UNCHANGED);
            }
            Some(VcswatchStatus::Unreleased) => {
                log::warn!(
                    "Ignoring package {} with version that is still UNRELEASED",
                    row.package
                );
                continue;
            }
            Some(VcswatchStatus::Todo) => {
                log::warn!("Ignoring package {} with status TODO", row.package);
                continue;
            }
            Some(VcswatchStatus::Error) => {
                if let Some(url) = row.url.as_ref() {
                    let mut parsed_url: url::Url = match url.parse() {
                        Ok(url) => url,
                        Err(e) => {
                            log::error!("Failed to parse URL {}: {}", url, e);
                            continue;
                        }
                    };
                    if debian_janitor::is_alioth_url(&parsed_url) {
                        if row.vcs == Some("git".to_owned()) {
                            candidate.distribution = Some(release.to_string());
                            candidate.package = Some(row.package.clone());
                            candidate.campaign = Some("alioth-imports".to_owned());

                            let path = parsed_url.path().trim_start_matches('/').to_string();

                            if path.starts_with("git/") {
                                parsed_url.set_path(&path[4..]);
                            }

                            if !path.ends_with(".git") {
                                parsed_url.set_path(&(path.to_owned() + ".git"));
                            }

                            candidate.command = Some(format!(
                                "import-alioth-archive.py https://alioth-archive.debian.org/git/{path}.tar.xz",
                                path = parsed_url.path()
                            ));

                            candidate.value = Some(DEFAULT_VALUE_IMPORT_FROM_ALIOTH);
                        } else {
                            candidate.distribution = Some(release.to_owned());
                            candidate.package = Some(row.package.clone());
                            candidate.campaign = Some("unversioned".to_owned());
                            candidate.value = Some(DEFAULT_VALUE_UNVERSIONED);
                            candidate
                                .extra_arg
                                .push(format!("--package={}", row.package));
                        }
                    } else {
                        log::warn!(
                            "Ignoring package {} with error: {}",
                            row.package,
                            row.error.unwrap()
                        );
                        continue;
                    }
                } else {
                    log::warn!(
                        "Ignoring package {} with error: {}",
                        row.package,
                        row.error.unwrap()
                    );
                    continue;
                }
            }
        }

        debian_janitor::candidates::print_candidate_as_textpb(candidate);
    }

    Ok(())
}

#[derive(Parser, Debug)]
struct Args {
    #[arg(short, long, value_name = "FILE")]
    config: Option<std::path::PathBuf>,

    #[command(flatten)]
    udd: debian_janitor::udd::UDDArgs,

    #[command(flatten)]
    logging: janitor::logging::LoggingArgs,

    packages: Option<Vec<String>>,
}
#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let args = Args::parse();

    args.logging.init();

    let pool = args.udd.connect().await?;

    let mut tags = std::collections::HashSet::new();
    for fixer in lintian_brush::available_lintian_fixers(None, None)? {
        tags.extend(fixer.lintian_tags().into_iter());
    }

    iter_candidates(
        &pool,
        "sid",
        args.packages
            .as_ref()
            .map(|ps| ps.iter().map(|s| s.as_str()).collect()),
    )
    .await?;

    Ok(())
}
