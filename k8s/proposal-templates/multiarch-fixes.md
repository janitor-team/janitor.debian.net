{% extends "base.md" %}
{% block codemod %}
Apply hints suggested by the [multi-arch hinter](https://wiki.debian.org/MultiArch/Hints).

{% for entry in codemod["applied-hints"] %}
{% set kind = entry.link.split("#")[-1] %}
* {{ entry.binary }}: {% if entry.action %}{{ entry.action }} This fixes: {{ entry.description }}. ([{{ kind }}]({{ entry.link }})){% else %}Fix: {{ entry.description }}. ([{{ kind }}]({{ entry.link }})){% endif %}

{% endfor %}

Note that in some cases, these multi-arch hints may trigger lintian warnings until the dependencies of the package support multi-arch. This is expected, see [the FAQ](https://janitor.debian.net/multiarch-fixes#why-does-lintian-warn) for details.
{% endblock %}
