use axum::{
    response::IntoResponse,
    routing::Router,
    routing::{get, post},
};
use clap::Parser;
use std::path::PathBuf;
use std::sync::Arc;
use url::Url;

#[derive(Parser)]
struct Args {
    #[clap(long)]
    /// Host to listen on
    host: String,

    #[clap(long, default_value = "8080")]
    /// Port to listen on
    port: u16,

    #[clap(long, default_value = "8090")]
    /// Public port to listen on
    public_port: u16,

    #[clap(long, default_value = "http://localhost:9912")]
    /// URL for publisher
    publisher_url: Option<Url>,

    #[clap(long, default_value = "http://localhost:9911")]
    /// URL for runner
    runner_url: Option<Url>,

    #[clap(long, default_value = "http://localhost:9914")]
    /// URL for archive
    archiver_url: Option<Url>,

    #[clap(long, default_value = "http://localhost:9920")]
    /// URL for differ
    differ_url: Option<Url>,

    #[clap(long)]
    /// Policy file to read
    policy: PathBuf,

    #[clap(long, default_value = "janitor.conf")]
    /// Path to configuration
    config: PathBuf,

    #[clap(long)]
    /// Ognibuild dep server URL
    dep_server_url: Option<Url>,

    #[clap(long)]
    /// Followup server URL
    followup_url: Option<Url>,

    #[clap(flatten)]
    logging: janitor::logging::LoggingArgs,
}

pub const DEFAULT_SUCCESS_CHANCE: f64 = 0.5;

async fn handle_ready() -> impl IntoResponse {
    "ok"
}

async fn handle_health() -> impl IntoResponse {
    "ok"
}

#[tokio::main]
async fn main() -> Result<(), i32> {
    let args = Args::parse();

    args.logging.init();

    let config = janitor::config::read_file(&args.config).map_err(|e| {
        log::error!("Failed to read configuration: {}", e);
        1
    })?;

    let pool = janitor::state::create_pool(&config).await.map_err(|e| {
        log::error!("Failed to create database pool: {}", e);
        1
    })?;

    let policy = debian_janitor::policy::read_policy(&args.policy).map_err(|e| {
        log::error!("Failed to read policy: {}", e);
        1
    })?;

    let router = Router::new()
        .route("/ready", get(handle_ready))
        .route("/health", get(handle_health));

    let tcp_listener = tokio::net::TcpListener::bind((args.host.clone(), args.port))
        .await
        .map_err(|e| {
            log::error!("Failed to bind to {}:{}: {}", args.host, args.port, e);
            1
        })?;

    tokio::spawn(async { axum::serve(tcp_listener, router).await.unwrap() });

    let public_router = debian_janitor_site::public_router(
        pool,
        args.publisher_url.unwrap(),
        args.runner_url.unwrap(),
        args.archiver_url.unwrap(),
        args.differ_url.unwrap(),
        args.dep_server_url,
        args.followup_url,
        policy,
        config,
    );

    let public_tcp_listener = tokio::net::TcpListener::bind((args.host.clone(), args.public_port))
        .await
        .map_err(|e| {
            log::error!(
                "Failed to bind to {}:{}: {}",
                args.host,
                args.public_port,
                e
            );
            1
        })?;

    tokio::spawn(async {
        axum::serve(public_tcp_listener, public_router)
            .await
            .unwrap()
    });

    todo!()
}
