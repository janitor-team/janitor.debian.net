pub mod backports;
pub mod debianize;
pub mod handlers;
pub mod lintian_fixes;
pub mod mia;
pub mod multiarch_hints;
pub mod new_upstream;
pub mod orphan;
pub mod scrub_obsolete;
pub mod uncommitted;
pub mod watch_fixes;

use axum::{
    extract::State,
    routing::{get, post},
    Router,
};
use std::sync::Arc;
use url::Url;

pub struct AppState {
    pub pool: sqlx::PgPool,

    pub publisher_url: Url,

    pub runner_url: Url,

    pub archiver_url: Url,

    pub differ_url: Url,

    pub dep_server_url: Option<Url>,

    pub followup_url: Option<Url>,

    pub policy: debian_janitor::policy::PolicyConfig,

    pub config: janitor::config::Config,
}

pub fn public_router(
    pool: sqlx::PgPool,
    publisher_url: Url,
    runner_url: Url,
    archiver_url: Url,
    differ_url: Url,
    dep_server_url: Option<Url>,
    followup_url: Option<Url>,
    policy: debian_janitor::policy::PolicyConfig,
    config: janitor::config::Config,
) -> Router {
    let state = Arc::new(AppState {
        pool,
        publisher_url,
        runner_url,
        archiver_url,
        differ_url,
        dep_server_url,
        followup_url,
        policy,
        config,
    });

    Router::new()
        .route("/", get(crate::handlers::index))
        .route("/pkg/:name", get(crate::handlers::package_overview))
        .route("/evaluate/:run_id", get(crate::handlers::evaluate_run))
        .route("/credentials", get(crate::handlers::credentials))
        .route("/ssh_keys", get(crate::handlers::ssh_keys))
        .route("/pgp_keys", get(crate::handlers::pgp_keys))
        .route("/pgp_keys.asc", get(crate::handlers::pgp_keys_asc))
        .route(
            "/archive-keyring.asc",
            get(crate::handlers::archive_keyring_asc),
        )
        .route(
            "/archive-keyring.gpg",
            get(crate::handlers::archive_keyring_gpg),
        )
        .route(
            "/:campaign/pkg/:name/:filename",
            get(crate::handlers::result_file),
        )
        .nest(
            "/lintian-fixes",
            crate::lintian_fixes::routes(state.clone()),
        )
        .nest(
            "/multiarch-hints",
            crate::multiarch_hints::routes(state.clone()),
        )
        .nest("/orphan", crate::orphan::routes(state.clone()))
        .nest("/mia", crate::mia::routes(state.clone()))
        .nest("/debianize", crate::debianize::routes(state.clone()))
        .nest(
            "/scrub-obsolete",
            crate::scrub_obsolete::routes(state.clone()),
        )
        .nest(
            "/fresh-releases",
            crate::new_upstream::fresh_releases_routes(state.clone()),
        )
        .nest(
            "/fresh-snapshots",
            crate::new_upstream::fresh_snapshots_routes(state.clone()),
        )
        .nest(
            "/bookworm-backports",
            crate::backports::routes(state.clone(), "bookworm"),
        )
        .nest(
            "/bullseye-backports",
            crate::backports::routes(state.clone(), "bullseye"),
        )
        .nest("/watch-fixes", crate::watch_fixes::routes(state.clone()))
        .nest("/uncommitted", crate::uncommitted::routes(state.clone()))
        .route(
            "/:campaign/merge-proposals",
            get(crate::handlers::merge_proposals),
        )
        .route(
            "/:campaign/merge-proposal",
            post(crate::handlers::merge_proposal),
        )
        .route("/:campaign/ready", get(crate::handlers::ready))
        .route("/:campaign/done", get(crate::handlers::done))
        .route(
            "/:campaign/maintainer",
            get(crate::handlers::maintainer_list),
        )
        .route("/:campaign/pkg", get(crate::handlers::package_list))
        .route("/git", get(crate::handlers::git_repo_list))
        .route("/bzr", get(crate::handlers::bzr_repo_list))
        .route("/unchanged", get(crate::handlers::apt_repo))
        .route(
            "/cupboard/maintainer/:maintainer",
            get(crate::handlers::cupboard_maintainer_overview),
        )
        .route(
            "/maintainer/:maintainer",
            get(crate::handlers::maintainer_overview),
        )
        .route("/m/", get(crate::handlers::maintainer_index))
        .route(
            "/m/:maintainer",
            get(crate::handlers::maintainer_overview_short),
        )
        .route(
            "/cupboard/pkg/",
            get(crate::handlers::cupboard_package_list),
        )
        .route(
            "/:campaign/pkg/:pkg/:run_id/:filename",
            get(crate::handlers::result_file),
        )
        .route("/:campaign/", get(crate::handlers::generic_start))
        .route(
            "/:campaign/candidates",
            get(crate::handlers::generic_candidates),
        )
        .route(
            "/:campaign/pkg/:pkg",
            get(crate::handlers::generic_pkg_overview),
        )
        .route(
            "/:campaign/pkg/:pkg/:run_id",
            get(crate::handlers::generic_run_overview),
        )
        .route("/github/callback", post(crate::handlers::github_callback))
        .route(
            "/cupboard/maintainer-stats",
            get(crate::handlers::cupboard_maintainer_stats),
        )
        .route(
            "/cupboard/vcs-regressions",
            get(crate::handlers::cupboard_vcs_regressions),
        )
        .route(
            "/cupboard/maintainer",
            get(crate::handlers::cupboard_maintainer_list),
        )
        .route(
            "/cupboard/pkg/:pkg",
            get(crate::handlers::cupboard_pkg_overview),
        )
        .route("/fresh-builds", get(crate::new_upstream::fresh_builds))
        .route("/fresh", get(crate::new_upstream::fresh_builds))
        .with_state(state.clone())
}
