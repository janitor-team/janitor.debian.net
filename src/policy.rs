use crate::overrides::OverrideConfig;
use crate::packages::Package;
use chrono::{DateTime, Duration, Utc};
use glob::Pattern as GlobPattern;
use janitor::config::Config;
use log::{error, info, warn};
use mailparse::{addrparse, MailAddr};
use protobuf::text_format;
use regex::Regex;
use reqwest::header::{HeaderMap, HeaderValue, ACCEPT, CONTENT_TYPE};
use reqwest::Client;

use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fs::File;
use std::io::Read;
use std::sync::RwLock;
use url::Url;

const ROLES: &[&str] = &["main", "pristine-tar", "upstream"];

pub use crate::generated::policy::{
    CampaignPolicy, ChangelogBehaviour, Match, Mode as PublishMode, Policy, PolicyConfig,
    ReviewPolicy, Stage,
};

pub fn read_policy(
    file_path: &std::path::Path,
) -> Result<PolicyConfig, Box<dyn std::error::Error>> {
    let mut file = File::open(file_path)?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;

    Ok(text_format::parse_from_str(&contents)?)
}

pub struct CompiledMatch {
    maintainer: Vec<GlobPattern>,
    source_package: Vec<GlobPattern>,
    uploader: Vec<GlobPattern>,
    vcs_url_regex: Vec<Regex>,
    in_base: Option<bool>,
    after_stage: Vec<Stage>,
    before_stage: Vec<Stage>,
}

impl CompiledMatch {
    fn matches(
        &self,
        package_name: &str,
        vcs_url: Option<&str>,
        package_maintainer: Option<&str>,
        package_uploaders: Option<&[&str]>,
        in_base: Option<bool>,
        release_stages: Option<&HashMap<String, DateTime<Utc>>>,
    ) -> Result<bool, Error> {
        if let Some(package_maintainer) = package_maintainer {
            let package_maintainer_email = match addrparse(package_maintainer)
                .map_err(|e| {
                    Error::MaintainerInvalid(package_maintainer.to_string(), e.to_string())
                })?
                .as_slice()
            {
                [MailAddr::Single(info)] => info.addr.clone(),
                _ => {
                    return Err(Error::MaintainerInvalid(
                        package_maintainer.to_string(),
                        "Expected single email".to_string(),
                    ))
                }
            };
            for maintainer_pat in self.maintainer.iter() {
                if !maintainer_pat.matches(&package_maintainer_email) {
                    return Ok(false);
                }
            }
        } else if !self.maintainer.is_empty() {
            return Ok(false);
        }
        if !self.uploader.is_empty() {
            if package_uploaders.is_none() {
                return Ok(false);
            }
            let package_uploader_emails = package_uploaders
                .unwrap_or_default()
                .iter()
                .map(|uploader| {
                    match addrparse(uploader)
                        .map_err(|e| Error::UploaderInvalid(uploader.to_string(), e.to_string()))?
                        .as_slice()
                    {
                        [MailAddr::Single(info)] => Ok(info.addr.clone()),
                        _ => Err(Error::UploaderInvalid(
                            uploader.to_string(),
                            "Expected single email".to_string(),
                        )),
                    }
                })
                .collect::<Result<Vec<String>, _>>()?;
            for uploader_pat in self.uploader.iter() {
                if !package_uploader_emails
                    .iter()
                    .any(|u| uploader_pat.matches(u))
                {
                    return Ok(false);
                }
            }
        }
        for source_package_pat in self.source_package.iter() {
            if !source_package_pat.matches(package_name) {
                return Ok(false);
            }
        }
        for vcs_url_regex in &self.vcs_url_regex {
            if let Some(vcs_url) = vcs_url {
                if !vcs_url_regex.is_match(vcs_url) {
                    return Ok(false);
                }
            } else {
                return Ok(false);
            }
        }
        if let Some(in_base_value) = self.in_base {
            if let Some(in_base) = in_base {
                if in_base_value != in_base {
                    return Ok(false);
                }
            }
        }
        let now = Utc::now();
        if let Some(release_stages) = release_stages {
            for before_stage in self.before_stage.iter() {
                let stage_name = if let Some(ref stage_name) = before_stage.stage_name {
                    stage_name
                } else {
                    return Err(Error::InvalidPolicy(
                        "before_stage must have a stage_name".into(),
                    ));
                };
                if let Some(threshold) = release_stages.get(stage_name) {
                    let mut threshold = *threshold;
                    if let Some(days_delta) = before_stage.days_delta {
                        let delta = Duration::days(days_delta as i64);
                        threshold = threshold
                            .checked_sub_signed(delta)
                            .ok_or(Error::InvalidPolicy("Duration overflow".to_string()))?;
                    }
                    if now > threshold {
                        return Ok(false);
                    }
                } else {
                    warn!("Ignoring reference to unknown stage {}", stage_name);
                }
            }
            for after_stage in self.after_stage.iter() {
                let stage_name = if let Some(ref stage_name) = after_stage.stage_name {
                    stage_name
                } else {
                    return Err(Error::InvalidPolicy(
                        "after_stage must have a stage_name".into(),
                    ));
                };
                if let Some(threshold) = release_stages.get(stage_name) {
                    let mut threshold = *threshold;
                    if let Some(days_delta) = after_stage.days_delta {
                        let delta = Duration::days(days_delta as i64);
                        threshold = threshold
                            .checked_add_signed(delta)
                            .ok_or(Error::InvalidPolicy("Duration overflow".to_string()))?;
                    }
                    if now < threshold {
                        return Ok(false);
                    }
                } else {
                    warn!("Ignoring reference to unknown stage {}", stage_name);
                }
            }
        } else {
            return Err(Error::InvalidPolicy(
                "no release stages passed in, unable to match on stages".into(),
            ));
        }
        Ok(true)
    }
}

impl TryFrom<&Match> for CompiledMatch {
    type Error = Error;

    fn try_from(r#match: &Match) -> Result<Self, Error> {
        let maintainer = r#match
            .maintainer
            .iter()
            .map(|m| GlobPattern::new(m))
            .collect::<Result<Vec<_>, _>>()
            .map_err(|e| Error::InvalidPolicy(format!("Invalid maintainer glob pattern: {}", e)))?;
        let source_package = r#match
            .source_package
            .iter()
            .map(|m| GlobPattern::new(m))
            .collect::<Result<Vec<_>, _>>()
            .map_err(|e| {
                Error::InvalidPolicy(format!("Invalid source package glob pattern: {}", e))
            })?;
        let uploader = r#match
            .uploader
            .iter()
            .map(|m| GlobPattern::new(m))
            .collect::<Result<Vec<_>, _>>()
            .map_err(|e| Error::InvalidPolicy(format!("Invalid uploader glob pattern: {}", e)))?;
        let vcs_url_regex = r#match
            .vcs_url_regex
            .iter()
            .map(|m| Regex::new(m))
            .collect::<Result<Vec<_>, _>>()
            .map_err(|e| Error::InvalidPolicy(format!("Invalid vcs_url_regex regex: {}", e)))?;
        Ok(CompiledMatch {
            maintainer,
            source_package,
            uploader,
            vcs_url_regex,
            in_base: r#match.in_base,
            before_stage: r#match.before_stage.clone(),
            after_stage: r#match.after_stage.clone(),
        })
    }
}

pub fn matches(
    r#match: &Match,
    package_name: &str,
    vcs_url: Option<&str>,
    package_maintainer: Option<&str>,
    package_uploaders: Option<&[&str]>,
    in_base: Option<bool>,
    release_stages: Option<&HashMap<String, DateTime<Utc>>>,
) -> Result<bool, Error> {
    let m = CompiledMatch::try_from(r#match)?;

    m.matches(
        package_name,
        vcs_url,
        package_maintainer,
        package_uploaders,
        in_base,
        release_stages,
    )
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::collections::HashMap;

    #[test]
    fn test_matches() {
        let r#match = r#"
        maintainer: "jelmer@debian.org"
        source_package: "bzr"
        vcs_url_regex: "https://code.launchpad.net/~.*"
        "#;
        let m = text_format::parse_from_str::<Match>(r#match).unwrap();
        assert!(matches(
            &m,
            "bzr",
            Some("https://code.launchpad.net/~jelmer/bzr/trunk"),
            Some("Jelmer Vernooij <jelmer@debian.org>"),
            Some(&[]),
            Some(false),
            Some(&HashMap::new())
        )
        .unwrap());
        assert!(matches(
            &m,
            "bzr",
            Some("https://code.launchpad.net/~jelmer/bzr/trunk"),
            Some("jelmer@debian.org"),
            Some(&[]),
            Some(false),
            Some(&HashMap::new())
        )
        .unwrap());
        assert!(!matches(
            &m,
            "bzr",
            Some("https://code.launchpad.net/~jelmer/bzr/trunk"),
            Some("joe@debian.org"),
            Some(&[]),
            Some(false),
            Some(&HashMap::new())
        )
        .unwrap());
    }
}

#[derive(Debug)]
pub struct ExpandedPolicy {
    pub per_branch: HashMap<String, (PublishMode, Option<i32>)>,
    pub command: Option<String>,
    pub manual_review: Option<ReviewPolicy>,
}

#[derive(Debug, PartialEq, Eq)]
pub enum Error {
    MaintainerInvalid(String, String),
    UploaderInvalid(String, String),
    InvalidPolicy(String),
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Error::InvalidPolicy(msg) => write!(f, "Invalid policy: {}", msg),
            Error::UploaderInvalid(uploader, error) => {
                write!(f, "Invalid uploader email address {}: {}", uploader, error)
            }
            Error::MaintainerInvalid(maintainer, error) => {
                write!(
                    f,
                    "Invalid maintainer email address {}: {}",
                    maintainer, error
                )
            }
        }
    }
}

impl std::error::Error for Error {}

pub fn expand_policy(
    config: &PolicyConfig,
    overrides: &OverrideConfig,
    campaign: &str,
    package_name: &str,
    vcs_url: Option<&str>,
    maintainer: Option<&str>,
    uploaders: &[String],
    in_base: Option<bool>,
    release_stages: Option<&HashMap<String, DateTime<Utc>>>,
) -> Result<ExpandedPolicy, Error> {
    let mut publish_mode: HashMap<String, (PublishMode, Option<i32>)> = HashMap::new();
    let mut env: HashMap<String, String> = HashMap::new();

    if campaign == "fresh-releases" || campaign == "fresh-snapshots" {
        for override_package in &overrides.package {
            if override_package.name.as_ref().unwrap() == package_name
                && override_package.upstream_branch_url.is_some()
            {
                env.insert(
                    "UPSTREAM_BRANCH_URL".to_string(),
                    override_package.upstream_branch_url.clone().unwrap(),
                );
            }
        }
    }

    let mut command: Option<String> = None;
    let mut manual_review: Option<ReviewPolicy> = None;

    for policy in config.policy.iter() {
        let mut is_match = false;
        for m in policy.match_.iter() {
            match matches(
                m,
                package_name,
                vcs_url,
                maintainer,
                Some(
                    uploaders
                        .iter()
                        .map(|u| u.as_str())
                        .collect::<Vec<_>>()
                        .as_slice(),
                ),
                in_base,
                release_stages,
            ) {
                Ok(false) => continue,
                Ok(true) => {
                    is_match = true;
                    break;
                }
                Err(e) => {
                    return Err(e);
                }
            }
        }
        if !is_match {
            continue;
        }

        if let Some(update_changelog) = &policy.update_changelog {
            env.insert(
                "DEB_UPDATE_CHANGELOG".to_string(),
                match update_changelog.unwrap() {
                    ChangelogBehaviour::update => "update".to_string(),
                    ChangelogBehaviour::leave => "leave".to_string(),
                    ChangelogBehaviour::auto => "auto".to_string(),
                },
            );
        }

        if let Some(compat_release) = &policy.compat_release {
            env.insert("DEB_COMPAT_RELEASE".to_string(), compat_release.clone());
        }

        for env_entry in &policy.env {
            if let Some(value) = &env_entry.value {
                env.insert(env_entry.name.clone().unwrap(), value.clone());
            } else {
                env.remove(&env_entry.name.clone().unwrap());
            }
        }

        for c in &policy.campaign {
            if c.name() != campaign {
                continue;
            }

            for publish in &c.publish {
                if publish.role.is_none() {
                    for role in ROLES {
                        publish_mode.insert(
                            role.to_string(),
                            (publish.mode.unwrap().unwrap(), publish.max_frequency_days),
                        );
                    }
                } else {
                    publish_mode.insert(
                        publish.role.clone().unwrap(),
                        (publish.mode.unwrap().unwrap(), publish.max_frequency_days),
                    );
                }
            }

            if let Some(cmd) = &c.command {
                command = Some(
                    env.iter()
                        .map(|(name, value)| format!("{}={}", name, value))
                        .chain(std::iter::once(cmd.clone()))
                        .collect::<Vec<String>>()
                        .join(" "),
                );
            }

            if let Some(review) = &c.manual_review {
                manual_review = Some(review.unwrap());
            }
        }
    }

    Ok(ExpandedPolicy {
        per_branch: publish_mode,
        command,
        manual_review,
    })
}

pub fn determine_command(
    policy: &PolicyConfig,
    overrides: &OverrideConfig,
    config: &Config,
    package_info: Option<&Package>,
    release_stages_passed: Option<&HashMap<String, DateTime<Utc>>>,
    campaign: &str,
    extra_args: &[String],
) -> Result<String, Error> {
    let mut command: String = if let Some(package_info) = package_info {
        let expanded: ExpandedPolicy = expand_policy(
            policy,
            overrides,
            campaign,
            package_info.name.as_str(),
            package_info.vcs_url.as_deref(),
            package_info.maintainer_email.as_deref(),
            package_info.uploader_emails.as_slice(),
            package_info.in_base,
            release_stages_passed,
        )?;
        expanded.command.unwrap()
    } else {
        let campaign = config.get_campaign(campaign);
        campaign.unwrap().command.clone().unwrap()
    };
    if !extra_args.is_empty() {
        let mut args = shlex::split(command.as_str()).unwrap_or_default();
        args.extend_from_slice(extra_args);
        command = shlex::try_join(args.iter().map(|s| s.as_str())).unwrap();
    }
    Ok(command)
}

#[derive(Debug)]
pub struct PolicyApplier {
    pub policy: PolicyConfig,
    pub overrides: OverrideConfig,
    pub release_stages: RwLock<Option<HashMap<String, DateTime<Utc>>>>,
}

impl Default for PolicyApplier {
    fn default() -> Self {
        Self::new()
    }
}

impl PolicyApplier {
    pub fn new() -> Self {
        Self {
            policy: PolicyConfig::default(),
            overrides: OverrideConfig::default(),
            release_stages: RwLock::new(None),
        }
    }

    pub fn from_paths(
        path: &std::path::Path,
        overrides_path: Option<&std::path::Path>,
    ) -> Result<Self, Error> {
        let policy = read_policy(path)
            .map_err(|e| Error::InvalidPolicy(format!("Failed to read policy: {}", e)))?;
        let overrides = if let Some(overrides_path) = overrides_path {
            crate::overrides::read_overrides(overrides_path)
                .map_err(|e| Error::InvalidPolicy(format!("Failed to read overrides: {}", e)))?
        } else {
            OverrideConfig::default()
        };
        Ok(Self {
            policy,
            overrides,
            release_stages: RwLock::new(None),
        })
    }

    pub fn expand(
        &self,
        campaign: &str,
        package_name: &str,
        vcs_url: Option<&str>,
        maintainer: Option<&str>,
        uploaders: Option<&[&str]>,
        in_base: Option<bool>,
    ) -> Result<ExpandedPolicy, Error> {
        let uploaders = if let Some(uploaders) = uploaders {
            uploaders.iter().map(|u| u.to_string()).collect::<Vec<_>>()
        } else {
            vec![]
        };
        expand_policy(
            &self.policy,
            &self.overrides,
            campaign,
            package_name,
            vcs_url,
            maintainer,
            &uploaders,
            in_base,
            self.release_stages.read().unwrap().as_ref(),
        )
    }

    pub async fn load_release_stages(&mut self) -> Result<(), crate::release::Error> {
        self.release_stages.write().unwrap().replace(
            if let Some(ref freeze_dates_url) = self.policy.freeze_dates_url {
                let release_stages = crate::release::read_release_stages(freeze_dates_url).await?;
                info!("Release stages passed: {:?}", release_stages);
                release_stages
            } else {
                std::collections::HashMap::new()
            },
        );
        Ok(())
    }

    pub fn known_campaigns(&self) -> Vec<String> {
        self.policy
            .policy
            .iter()
            .flat_map(|p| p.campaign.iter())
            .map(|c| c.name.clone().unwrap())
            .collect()
    }
}

pub fn publish_policy_name(campaign: &str, package: &str) -> String {
    format!("{}-{}", campaign, package)
}

pub fn validate_policy(
    config: &janitor::config::Config,
    policy: &PolicyConfig,
) -> Result<(), String> {
    let mut campaigns = std::collections::HashSet::new();
    for campaign in &config.campaign {
        campaigns.insert(campaign.name.as_ref().unwrap().clone());
    }
    for p in &policy.policy {
        if let Some(compat_release) = &p.compat_release {
            if debian_analyzer::release_info::resolve_release_codename(compat_release, None)
                .is_none()
            {
                return Err(format!("invalid compat release {}", compat_release));
            }
        }
        for env_entry in &p.env {
            if env_entry.name.is_none() {
                return Err("env entry missing name".to_string());
            }
        }
        for s in &p.campaign {
            let name = s.name.as_ref().unwrap();
            if !campaigns.contains(name) {
                return Err(format!("unknown campaign {}", name));
            }
        }
    }
    Ok(())
}

impl ToString for PublishMode {
    fn to_string(&self) -> String {
        match &self {
            PublishMode::push => "push".to_string(),
            PublishMode::propose => "propose".to_string(),
            PublishMode::attempt_push => "attempt-push".to_string(),
            PublishMode::bts => "bts".to_string(),
            PublishMode::skip => "skip".to_string(),
            PublishMode::push_derived => "push-derived".to_string(),
            PublishMode::build_only => "build-only".to_string(),
        }
    }
}

fn serialize_publish_mode<S: serde::Serializer>(
    mode: &PublishMode,
    serializer: S,
) -> Result<S::Ok, S::Error> {
    serializer.serialize_str(&mode.to_string())
}

fn deserialize_publish_mode<'de, D>(deserializer: D) -> Result<PublishMode, D::Error>
where
    D: serde::Deserializer<'de>,
{
    let value = String::deserialize(deserializer)?;
    match value.as_str() {
        "push" => Ok(PublishMode::push),
        "propose" => Ok(PublishMode::propose),
        "attempt-push" => Ok(PublishMode::attempt_push),
        "bts" => Ok(PublishMode::bts),
        "skip" => Ok(PublishMode::skip),
        "push-derived" => Ok(PublishMode::push_derived),
        "build-only" => Ok(PublishMode::build_only),
        _ => Err(serde::de::Error::custom(format!(
            "unknown publish mode {}",
            value
        ))),
    }
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq)]
pub struct BranchPublishPolicy {
    #[serde(
        deserialize_with = "deserialize_publish_mode",
        serialize_with = "serialize_publish_mode"
    )]
    mode: PublishMode,
    max_frequency_days: Option<u32>,
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq)]
pub struct CodebasePublishPolicy {
    rate_limit_bucket: Option<String>,
    per_branch: HashMap<String, BranchPublishPolicy>,
}

pub type PublishPolicy = HashMap<String, CodebasePublishPolicy>;

pub async fn upload_publish_policy(
    session: &Client,
    publisher_url: &Url,
    name: &str,
    serialized_publish_policy: &CodebasePublishPolicy,
) -> Result<(), reqwest::Error> {
    let url = publisher_url.join(&format!("policy/{}", name)).unwrap();
    let serialized = serde_json::to_value(serialized_publish_policy).unwrap();
    let mut headers = HeaderMap::new();
    headers.insert(CONTENT_TYPE, HeaderValue::from_static("application/json"));
    headers.insert(ACCEPT, HeaderValue::from_static("application/json"));
    let request = session.put(url).headers(headers).json(&serialized);
    request.send().await?;
    Ok(())
}

#[derive(Debug)]
pub enum SyncError {
    FetchFailure(reqwest::StatusCode),
    PackageIterFailure(sqlx::Error),
}

impl std::fmt::Display for SyncError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            SyncError::FetchFailure(status) => write!(f, "fetch failure: {}", status),
            SyncError::PackageIterFailure(e) => write!(f, "package iter failure: {}", e),
        }
    }
}

pub async fn sync_publish_policy(
    policy_applier: &PolicyApplier,
    current_policy: &mut PublishPolicy,
    session: &Client,
    publisher_url: &Url,
    campaign: &str,
    package: &Package,
) -> Result<bool, Error> {
    let intended_policy = policy_applier.expand(
        campaign,
        package.name.as_str(),
        package.vcs_url.as_deref(),
        package.maintainer_email.as_deref(),
        Some(
            package
                .uploader_emails
                .iter()
                .map(|s| s.as_str())
                .collect::<Vec<_>>()
                .as_slice(),
        ),
        package.in_base,
    )?;
    let name = publish_policy_name(campaign, &package.name);
    let intended_publish_policy = CodebasePublishPolicy {
        rate_limit_bucket: package.maintainer_email.clone(),
        per_branch: intended_policy
            .per_branch
            .iter()
            .map(|(k, v)| {
                (
                    k.clone(),
                    BranchPublishPolicy {
                        mode: v.0,
                        max_frequency_days: v.1.map(|i| i as u32),
                    },
                )
            })
            .collect(),
    };
    let stored_policy = current_policy.remove(&name);
    if stored_policy.is_none() || stored_policy.as_ref() != Some(&intended_publish_policy) {
        upload_publish_policy(session, publisher_url, &name, &intended_publish_policy)
            .await
            .unwrap();
        Ok(true)
    } else {
        Ok(false)
    }
}

pub async fn sync_publish_policies(
    conn: &mut sqlx::PgConnection,
    policy_applier: &PolicyApplier,
    publisher_url: &Url,
    selected_package: Option<&str>,
) -> Result<(usize, Option<usize>), SyncError> {
    let campaigns = policy_applier.known_campaigns();
    let mut num_updated = 0;

    info!("Creating current policy");
    let session = Client::new();
    let policy_url = publisher_url.join("policy").unwrap();
    let resp = session.get(policy_url).send().await.unwrap();
    let mut current_policy: PublishPolicy = if resp.status().is_success() {
        resp.json().await.unwrap()
    } else {
        return Err(SyncError::FetchFailure(resp.status()));
    };

    info!("Current policy: {} entries", current_policy.len());
    info!("Updating policy");

    let packages = crate::packages::iter_packages(conn, selected_package)
        .await
        .map_err(SyncError::PackageIterFailure)?;

    for package in packages {
        let mut updated = false;
        for campaign in &campaigns {
            match sync_publish_policy(
                policy_applier,
                &mut current_policy,
                &session,
                publisher_url,
                campaign,
                &package,
            )
            .await
            {
                Ok(true) => updated = true,
                Ok(false) => (),
                Err(e) => {
                    error!("Error syncing policy for {}: {}", package.name, e);
                }
            }
        }
        if updated {
            num_updated += 1;
        }
    }

    let num_deleted;
    if selected_package.is_some() {
        num_deleted = None;
    } else {
        num_deleted = Some(current_policy.len());
        info!("Deleting {} from policy", current_policy.len());
        for name in current_policy.keys() {
            let delete_url = publisher_url.join(&format!("policy/{}", name)).unwrap();
            let request = session.delete(delete_url);
            if let Err(e) = request.send().await {
                if let Some(status) = e.status() {
                    if status == 412 {
                        info!("Conflict removing policy {:?}", name);
                        continue;
                    }
                }
                panic!("Error occurred while deleting policy: {:?}", e);
            }
        }
    }

    Ok((num_updated, num_deleted))
}
