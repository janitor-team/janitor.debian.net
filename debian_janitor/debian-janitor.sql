CREATE EXTENSION IF NOT EXISTS debversion;
CREATE OR REPLACE VIEW absorbed_multiarch_hints AS
  select codebase, id, x->>'binary' as binary, x->>'link'::text as link, x->>'severity' as severity, x->>'source' as source, (x->>'version')::debversion as version, x->'action' as action, x->>'certainty' as certainty from (select codebase, id, jsonb_array_elements(result->'applied-hints') as x from absorbed_runs where campaign = 'multiarch-fixes') as f;

CREATE OR REPLACE VIEW lintian_results AS
   select run_id, path, name, context, severity from debian_build, json_to_recordset(lintian_result->'groups'->0->'input-files') as entries(path text, tags json), json_to_recordset(tags) as hint(context text, name text, severity text);
CREATE OR REPLACE VIEW absorbed_lintian_fixes AS
  select absorbed_runs.*, x.summary, x.description as fix_description, x.certainty, x.fixed_lintian_tags from absorbed_runs, json_to_recordset((result->'applied')::json) as x("summary" text, "description" text, "certainty" text, "fixed_lintian_tags" text[]);

CREATE OR REPLACE VIEW last_unabsorbed_lintian_fixes AS
  select last_unabsorbed_runs.*, x.summary, x.description as fix_description, x.certainty, x.fixed_lintian_tags from last_unabsorbed_runs, json_to_recordset((result->'applied')::json) as x("summary" text, "description" text, "certainty" text, "fixed_lintian_tags" text[]) WHERE result_code = 'success';

-- This appears to be unused
CREATE OR REPLACE VIEW multiarch_hints AS
  select codebase, id, x->>'binary' as binary, x->>'link'::text as link, x->>'severity' as severity, x->>'source' as source, (x->>'version')::debversion as version, x->'action' as action, x->>'certainty' as certainty from (select codebase, id, json_array_elements(result->'applied-hints') as x from run where suite = 'multiarch-fixes') as f;
CREATE TYPE vcswatch_status AS ENUM('ok', 'error', 'old', 'new', 'commits', 'unrel');
CREATE DOMAIN debian_package_name AS TEXT check (value similar to '[a-z0-9][a-z0-9+-.]+');
CREATE DOMAIN distribution_name AS TEXT check (value similar to '[a-z0-9][a-z0-9+-.]+');
CREATE TABLE IF NOT EXISTS package (
   name debian_package_name not null,
   distribution distribution_name not null,

   codebase text not null references codebase(name),

   -- TODO(jelmer): Move these to codebase
   vcs_type vcs_type,
   branch_url text,
   subpath text,

   archive_version debversion,
   vcs_url text,
   vcs_browse text,
   origin text,
   unique(distribution, name)
);
CREATE INDEX ON package (vcs_url);
CREATE INDEX ON package (branch_url);

ALTER TABLE package ADD vcswatch_status vcswatch_status;
ALTER TABLE package ADD vcswatch_version debversion;
-- ALTER TABLE package ADD archive_version debversion;
ALTER TABLE package ADD uploader_emails text[] not null default array[]::text[];
ALTER TABLE package ADD maintainer_email text;
ALTER TABLE package ADD in_base boolean;
ALTER TABLE package ADD removed boolean default false;
CREATE INDEX ON package (removed);

CREATE TYPE repology_status AS ENUM('newest', 'unique', 'outdated', 'ignored', 'rolling', 'legacy', 'noscheme', 'incorrect', 'untrusted', 'devel');
CREATE TABLE repology_project_repo (
    project text not null,
    repo text not null,
    srcname text not null,
    version text not null,
    status repology_status not null,
    unique (project, repo)
);

CREATE OR REPLACE FUNCTION drop_candidates_for_deleted_packages()
  RETURNS TRIGGER
  LANGUAGE PLPGSQL
  AS
$$
BEGIN
    IF NEW.removed AND NOT OLD.removed THEN
        DELETE FROM candidate WHERE codebase = NEW.codebase;
    END IF;

    RETURN NEW;
END;
$$;

CREATE OR REPLACE TRIGGER drop_candidates_when_removed
  AFTER UPDATE OF removed
  ON package
  FOR EACH ROW
  EXECUTE FUNCTION drop_candidates_for_deleted_packages();

create view last_missing_apt_dependencies as select id, codebase, suite, relation.* from last_unabsorbed_runs, json_array_elements(failure_details->'relations') as relations, json_to_recordset(relations) as relation(name text, archqual text, version text[], arch text, restrictions text) where result_code = 'unsatisfied-apt-dependencies';


-- TODO(jelmer): Move to Debian janitor
CREATE EXTENSION IF NOT EXISTS debversion;
CREATE TABLE IF NOT EXISTS upstream (
   name text,
   upstream_branch_url text,
   primary key(name)
);

CREATE VIEW all_debian_versions AS
SELECT
  source,
  distribution,
  version
FROM
  debian_build

UNION

SELECT
  name AS source,
  distribution,
  archive_version AS version
FROM
  package;

CREATE OR REPLACE VIEW upstream_branch_urls as (
    select package.name AS package, result->>'upstream_branch_url' as url from run left join package on run.codebase = package.codebase where suite in ('fresh-snapshots', 'fresh-releases') and result->>'upstream_branch_url' != '')
union
    (select name as package, upstream_branch_url as url from upstream);


