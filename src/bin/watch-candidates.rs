use clap::Parser;

use debian_janitor::candidates::Candidate;

use chrono::NaiveDateTime;

use sqlx::postgres::PgPool;
use sqlx::Arguments;
use std::error::Error;

const DEFAULT_VALUE_NEW_UPSTREAM: i32 = 30;
const DEFAULT_VALUE_WATCH_FIXES: i32 = 50;

#[derive(Parser, Debug)]
struct Args {
    #[arg(short, long, value_name = "FILE")]
    config: Option<std::path::PathBuf>,

    #[command(flatten)]
    udd: debian_janitor::udd::UDDArgs,

    #[command(flatten)]
    logging: janitor::logging::LoggingArgs,

    packages: Option<Vec<String>>,
}

#[derive(sqlx::Type, Debug, Clone, Copy)]
#[sqlx(type_name = "text")]
enum WatchStatus {
    #[sqlx(rename = "Newer version available")]
    NewerVersionAvailable,

    #[sqlx(rename = "package available")]
    PackageAvailable,

    #[sqlx(rename = "error")]
    Error,

    #[sqlx(rename = "only older package available")]
    OnlyOlderPackageAvailable,

    #[sqlx(rename = "Debian version newer than remote site")]
    DebianVersionNewerThanRemoteSite,

    #[sqlx(rename = "up to date")]
    UpToDate,
}

#[derive(sqlx::FromRow, Debug, Clone)]
struct WatchRow {
    source: String,
    version: debversion::Version,
    status: Option<WatchStatus>,
    last_check: Option<NaiveDateTime>,
    errors: Option<String>,
    warnings: Option<String>,
}

pub async fn iter_candidates(
    pool: &PgPool,
    release: &str,
    packages: Option<Vec<&str>>,
) -> Result<(), Box<dyn Error>> {
    let mut args = sqlx::postgres::PgArguments::default();
    args.add(release);

    let mut query = "
        SELECT source, version, status, errors, warnings, last_check
        FROM upstream_status
        WHERE release = $1
    "
    .to_owned();

    if let Some(packages) = packages {
        query += " AND source = ANY($2::text[])";
        args.add(&packages);
    }

    let rows: Vec<WatchRow> = sqlx::query_as_with(&query, args)
        .fetch_all(pool)
        .await
        .expect("Failed to fetch candidates");

    for row in rows {
        let mut candidate = Candidate::default();

        match row.status {
            Some(WatchStatus::NewerVersionAvailable) | Some(WatchStatus::PackageAvailable) => {
                candidate.distribution = Some(release.to_owned());
                candidate.package = Some(row.source);
                candidate.context = Some(row.version.to_string());
                candidate.campaign = Some("fresh-releases".to_owned());
                candidate.value = Some(DEFAULT_VALUE_NEW_UPSTREAM);
            }
            Some(WatchStatus::Error) | Some(WatchStatus::OnlyOlderPackageAvailable) => {
                candidate.distribution = Some(release.to_owned());
                candidate.campaign = Some("watch-fixes".to_owned());
                candidate.package = Some(row.source);
                candidate.context = row.last_check.map(|dt| dt.to_string());
                candidate.value = Some(DEFAULT_VALUE_WATCH_FIXES);
            }
            Some(WatchStatus::DebianVersionNewerThanRemoteSite) => {
                candidate.distribution = Some(release.to_owned());
                candidate.campaign = Some("watch-fixes".to_owned());
                candidate.package = Some(row.source);
                candidate.context = row.last_check.map(|dt| dt.to_string());
                candidate.value = Some(DEFAULT_VALUE_WATCH_FIXES);
            }
            Some(WatchStatus::UpToDate) => continue,
            None => {
                log::warn!("No status known for {}", row.source);
                continue;
            }
        }

        debian_janitor::candidates::print_candidate_as_textpb(candidate);
    }
    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let args = Args::parse();

    args.logging.init();

    let pool = args.udd.connect().await?;

    let mut tags = std::collections::HashSet::new();
    for fixer in lintian_brush::available_lintian_fixers(None, None)? {
        tags.extend(fixer.lintian_tags().into_iter());
    }

    iter_candidates(
        &pool,
        "sid",
        args.packages
            .as_ref()
            .map(|ps| ps.iter().map(|s| s.as_str()).collect()),
    )
    .await?;

    Ok(())
}
