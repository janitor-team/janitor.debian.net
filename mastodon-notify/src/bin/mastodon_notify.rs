use futures::stream::StreamExt;
use log::{error, info, warn};
use std::error::Error;
use std::path::{Path, PathBuf};
use std::sync::Arc;

use axum::http::StatusCode;
use axum::routing::get;
use clap::Parser;
use lazy_static::lazy_static;
use mastodon_async::helpers::toml;
use mastodon_async::prelude::*;
use mastodon_async::{helpers::cli, Result};
use prometheus::{register_counter, Counter};
use redis::Client as RedisClient;

lazy_static! {
    static ref TOOTS_POSTED: Counter =
        register_counter!("toots_posted", "Number of toots posted").unwrap();
}

struct MastodonNotifier {
    mastodon: Arc<Mastodon>,
}

#[derive(Debug, serde::Deserialize)]
struct LintianFixesResult {
    applied: Option<Vec<serde_json::Value>>,
}

impl MastodonNotifier {
    fn new(mastodon: Arc<Mastodon>) -> Self {
        MastodonNotifier { mastodon }
    }

    async fn toot(&self, msg: &str) -> Result<()> {
        TOOTS_POSTED.inc();

        self.mastodon
            .new_status(NewStatus {
                status: Some(msg.to_string()),
                ..Default::default()
            })
            .await?;
        Ok(())
    }

    async fn handle_publish(&self, publish: &janitor::publish::PublishNotification) -> Result<()> {
        if publish.mode == janitor::publish::PublishMode::Push && publish.result_code == "success" {
            let url = publish
                .main_branch_web_url
                .as_ref()
                .unwrap_or_else(|| publish.main_branch_url.as_ref().unwrap());
            let mut msg = format!(
                "Pushed #{} changes to {} ({})",
                publish.campaign, url, publish.codebase
            );

            if publish.campaign == "lintian-fixes" {
                let result: LintianFixesResult =
                    serde_json::from_value(publish.result.clone()).unwrap();
                if let Some(applied) = &result.applied {
                    let tags: Vec<&str> = applied
                        .iter()
                        .flat_map(|entry| entry["fixed_lintian_tags"].as_array())
                        .flatten()
                        .map(|tag| tag.as_str().unwrap_or(""))
                        .collect();

                    if !tags.is_empty() {
                        msg += &format!(", fixing: {}.", tags.join(", "));
                    }
                }
            }

            self.toot(&msg).await?;
        }
        Ok(())
    }

    async fn handle_merge_proposal(
        &self,
        merge_proposal: &janitor::publish::MergeProposalNotification,
    ) -> Result<()> {
        if merge_proposal.status == janitor::publish::MergeProposalStatus::Merged {
            let msg = format!(
                "Merge proposal {} ({}) merged{}.",
                merge_proposal.url,
                merge_proposal.codebase,
                merge_proposal
                    .merged_by
                    .as_ref()
                    .map(|m| format!(" by {}", m))
                    .unwrap_or_else(|| "".to_string())
            );

            self.toot(&msg).await?;
        }
        Ok(())
    }
}

async fn register(client: reqwest::Client, base_url: url::Url, path: &Path) -> Result<Mastodon> {
    let registration = Registration::new_with_client(base_url, client)
        .client_name("debian-janitor-notify")
        .build()
        .await?;
    let mastodon = cli::authenticate(registration).await?;

    // Save app data for using on the next run.
    toml::to_file(&mastodon.data, path)?;

    Ok(mastodon)
}

async fn health_check() -> &'static str {
    "Server is healthy"
}

async fn metrics() -> (StatusCode, String) {
    let encoder = prometheus::TextEncoder::new();
    let metric_families = prometheus::gather();
    match encoder.encode_to_string(&metric_families) {
        Ok(s) => (StatusCode::OK, s),
        Err(e) => (
            StatusCode::INTERNAL_SERVER_ERROR,
            format!("Error encoding metrics: {}", e),
        ),
    }
}

#[tokio::main]
async fn main() -> std::result::Result<(), Box<dyn Error>> {
    let args = Args::parse();

    args.logging.init();

    let client = reqwest::Client::builder()
        .user_agent(concat!(
            env!("CARGO_PKG_NAME"),
            "/",
            env!("CARGO_PKG_VERSION")
        ))
        .build()?;

    let mastodon = if let Ok(data) = toml::from_file(args.mastodon_data.as_path()) {
        Mastodon::new(client, data)
    } else {
        register(client, args.api_base_url, args.mastodon_data.as_ref()).await?
    };

    let you = mastodon.verify_credentials().await?;

    info!("Logged in as {}", you.acct);

    let redis_client = RedisClient::open(args.redis_url)?;
    let redis_conn = redis_client.get_async_connection().await?;

    let mut pubsub = redis_conn.into_pubsub();
    pubsub.subscribe("publish").await?;
    pubsub.subscribe("merge-proposal").await?;

    let notifier = Arc::new(MastodonNotifier::new(Arc::new(mastodon.clone())));

    tokio::spawn(async move {
        let mut stream = pubsub.on_message();

        while let Some(msg) = stream.next().await {
            let channel = msg.get_channel_name();
            let payload = msg.get_payload::<String>().unwrap();

            let r = match channel {
                "merge-proposal" => {
                    let data: janitor::publish::MergeProposalNotification =
                        serde_json::from_str(&payload).unwrap();
                    notifier.handle_merge_proposal(&data).await
                }
                "publish" => {
                    let data: janitor::publish::PublishNotification =
                        serde_json::from_str(&payload).unwrap();
                    notifier.handle_publish(&data).await
                }
                _ => {
                    warn!("Unknown channel: {}", channel);
                    continue;
                }
            };

            if let Err(e) = r {
                error!("Error handling {}: {}", channel, e);
            }
        }
    });

    let app = axum::Router::new()
        .route("/health", get(health_check))
        .route("/metrics", get(metrics));

    let addr = std::net::SocketAddr::new(
        args.prometheus_listen_address.parse().unwrap(),
        args.prometheus_port,
    );

    // Serve the app
    println!("Server running at http://{}", addr);
    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .unwrap();

    Ok(())
}

#[derive(Debug, Parser)]
struct Args {
    #[arg(long, env = "REDIS_URL", value_name = "URL")]
    redis_url: url::Url,

    /// The base URL of the Mastodon instance to connect to.
    #[arg(long, default_value = "https://mastodon.cloud/", value_name = "URL")]
    api_base_url: url::Url,

    // Mastodon data path
    #[arg(
        long,
        env = "MASTODON_DATA",
        value_name = "PATH",
        default_value = "mastodon-data.toml"
    )]
    mastodon_data: PathBuf,

    /// The address to listen on for Prometheus metrics.
    #[arg(long, default_value = "127.0.0.1", value_name = "LISTEN-ADDRESS")]
    prometheus_listen_address: String,

    /// The port to listen on for Prometheus metrics.
    #[arg(long, default_value = "9919", value_name = "PORT")]
    prometheus_port: u16,

    #[command(flatten)]
    logging: janitor::logging::LoggingArgs,
}
