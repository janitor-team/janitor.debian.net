use clap::{Arg, Command};
use debian_janitor::repology::{load_from_repology, Project};
use log::info;
use serde::{Deserialize, Serialize};
use sqlx::postgres::{PgConnectOptions, PgPool};
use sqlx::Error;
use std::error::Error as StdError;
use tokio_stream::StreamExt;

#[derive(Debug, Deserialize, Serialize)]
struct Config {
    database_location: String,
    user_agent: String,
}

async fn store_repology(pool: &mut PgPool, name: &str, repos: Vec<Project>) -> Result<(), Error> {
    info!("Storing {} repos for {}", repos.len(), name);

    let mut tx = pool.begin().await?;
    sqlx::query("DELETE FROM repology_project_repo WHERE project = $1")
        .bind(name)
        .execute(&mut *tx)
        .await?;

    for repo in repos {
        if repo.srcname.is_none() {
            log::debug!("No srcname in {:?}", repo);
            continue;
        }

        let query = sqlx::query(
        "INSERT INTO repology_project_repo (project, repo, srcname, version, status) VALUES ($1, $2, $3, $4, $5) ON CONFLICT (project, repo) DO UPDATE SET srcname = EXCLUDED.srcname, version = EXCLUDED.version, status = EXCLUDED.status"
    );
        query
            .bind(name.to_string())
            .bind(repo.repo.clone())
            .bind(repo.srcname.clone())
            .bind(repo.version.clone())
            .bind(repo.status)
            .execute(&mut *tx)
            .await?;
    }

    tx.commit().await?;
    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn StdError>> {
    let matches = Command::new("refresh-policy")
        .arg(
            Arg::new("config")
                .long("config")
                .value_name("FILE")
                .help("Path to configuration."),
        )
        .arg(
            Arg::new("debug")
                .long("debug")
                .action(clap::ArgAction::SetTrue)
                .help("Enable debug mode."),
        )
        .arg(
            Arg::new("gcp-logging")
                .long("gcp-logging")
                .action(clap::ArgAction::SetTrue)
                .help("Use Google Cloud Logging."),
        )
        .get_matches();

    let config_path: Option<&String> = matches.get_one("config");
    let debug_mode = matches.get_flag("debug");
    let gcp_logging = matches.get_flag("gcp-logging");

    if gcp_logging {
        stackdriver_logger::init_with_cargo!("../../Cargo.toml");
    } else if debug_mode {
        env_logger::init();
    } else {
        env_logger::builder()
            .filter(None, log::LevelFilter::Info)
            .init();
    }

    use janitor::config::read_file;
    let config_file = config_path.map(std::path::Path::new);

    let mut pool = if let Some(config_file) = config_file {
        let config = read_file(config_file)?;

        PgPool::connect(&config.database_location.unwrap()).await?
    } else {
        PgPool::connect_with(PgConnectOptions::new()).await.unwrap()
    };

    let stream = load_from_repology();
    tokio::pin!(stream);

    while let Some(item) = stream.next().await {
        match item {
            Ok((name, project)) => {
                store_repology(&mut pool, name.as_str(), project).await?;
            }
            Err(e) => {
                log::error!("Error: {:?}", e);
            }
        }
    }

    Ok(())
}
