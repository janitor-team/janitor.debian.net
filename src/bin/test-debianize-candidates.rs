use clap::Parser;

use debian_janitor::candidates::Candidate;

use sqlx::postgres::PgPool;
use sqlx::Arguments;
use std::error::Error;

#[derive(Parser, Debug)]
struct Args {
    #[arg(short, long, value_name = "FILE")]
    config: Option<std::path::PathBuf>,

    #[command(flatten)]
    udd: debian_janitor::udd::UDDArgs,

    #[command(flatten)]
    logging: janitor::logging::LoggingArgs,

    packages: Option<Vec<String>>,
}

const DEFAULT_VALUE_NEW: i32 = 50;
const DEFAULT_VALUE_ALREADY_PACKAGED: i32 = 25;

#[derive(sqlx::FromRow)]
struct UpstreamRow {
    name: String,
    vcs_url: String,
}

pub async fn iter_upstream_codebases(
    pool: &PgPool,
    packages: Option<Vec<&str>>,
) -> Result<(), Box<dyn Error>> {
    let mut args = sqlx::postgres::PgArguments::default();

    let mut query = "
        SELECT DISTINCT ON (sources.source)
            sources.source AS name,
            upstream_metadata.value AS vcs_url,
            ''
        FROM
            sources
            LEFT JOIN upstream_metadata ON upstream_metadata.source = sources.source
        WHERE
            sources.release = 'sid' AND
            upstream_metadata.key = 'Repository'
    "
    .to_owned();

    if let Some(packages) = packages {
        query += " AND sources.source = ANY($1::text[])";
        args.add(&packages);
    }

    query += " ORDER BY sources.source, sources.version DESC";

    let rows: Vec<UpstreamRow> = sqlx::query_as_with(&query, args)
        .fetch_all(pool)
        .await
        .expect("Failed to fetch candidates");

    for row in rows {
        let candidate = Candidate {
            distribution: Some("upstream".to_owned()),
            package: Some(row.name),
            command: Some(format!(
                "debianize {}",
                breezyshim::debian::directory::vcs_git_url_to_bzr_url(&row.vcs_url),
            )),
            campaign: Some("test-debianize".to_owned()),
            value: Some(DEFAULT_VALUE_ALREADY_PACKAGED),
            origin: Some("UDD".to_owned()),
            ..Default::default()
        };

        debian_janitor::candidates::print_candidate_as_textpb(candidate);
    }

    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let args = Args::parse();

    args.logging.init();

    let pool = args.udd.connect().await?;

    let mut tags = std::collections::HashSet::new();
    for fixer in lintian_brush::available_lintian_fixers(None, None)? {
        tags.extend(fixer.lintian_tags().into_iter());
    }

    iter_upstream_codebases(
        &pool,
        args.packages
            .as_ref()
            .map(|ps| ps.iter().map(|s| s.as_str()).collect()),
    )
    .await?;

    Ok(())
}
