use axum::{
    extract::State,
    response::IntoResponse,
    routing::Router,
    routing::{get, post},
};
use crate::AppState;
use std::sync::Arc;

pub async fn fresh_builds() -> impl IntoResponse {
    todo!()
}

pub async fn package_overview() -> impl IntoResponse {
    todo!()
}

pub async fn evaluate_run() -> impl IntoResponse {
    todo!()
}

pub async fn credentials() -> impl IntoResponse {
    todo!()
}

pub async fn ssh_keys() -> impl IntoResponse {
    todo!()
}

pub async fn pgp_keys() -> impl IntoResponse {
    todo!()
}

pub async fn pgp_keys_asc() -> impl IntoResponse {
    todo!()
}

pub async fn archive_keyring_asc() -> impl IntoResponse {
    todo!()
}

pub async fn archive_keyring_gpg() -> impl IntoResponse {
    todo!()
}

pub async fn result_file() -> impl IntoResponse {
    todo!()
}

pub async fn package_list() -> impl IntoResponse {
    todo!()
}

pub async fn maintainer_overview() -> impl IntoResponse {
    todo!()
}

pub async fn cupboard_package_list() -> impl IntoResponse {
    todo!()
}

pub async fn git_repo_list() -> impl IntoResponse {
    todo!()
}

pub async fn bzr_repo_list() -> impl IntoResponse {
    todo!()
}

pub async fn apt_repo() -> impl IntoResponse {
    todo!()
}

pub async fn maintainer_index() -> impl IntoResponse {
    todo!()
}

pub async fn maintainer_overview_short() -> impl IntoResponse {
    todo!()
}

pub async fn generic_candidates() -> impl IntoResponse {
    todo!()
}

pub async fn generic_pkg_overview() -> impl IntoResponse {
    todo!()
}

pub async fn generic_run_overview() -> impl IntoResponse {
    todo!()
}

pub async fn cupboard_maintainer_stats() -> impl IntoResponse {
    todo!()
}

pub async fn cupboard_vcs_regressions() -> impl IntoResponse {
    todo!()
}

pub async fn cupboard_maintainer_list() -> impl IntoResponse {
    todo!()
}

pub async fn cupboard_pkg_overview() -> impl IntoResponse {
    todo!()
}

pub async fn github_callback() -> impl IntoResponse {
    todo!()
}

pub async fn generic_start() -> impl IntoResponse {
    todo!()
}

pub async fn cupboard_maintainer_overview() -> impl IntoResponse {
    todo!()
}

pub async fn merge_proposal() -> impl IntoResponse {
    todo!()
}

pub async fn merge_proposals() -> impl IntoResponse {
    todo!()
}

pub async fn ready() -> impl IntoResponse {
    todo!()
}

pub async fn done() -> impl IntoResponse {
    todo!()
}

pub async fn maintainer_list() -> impl IntoResponse {
    todo!()
}

pub async fn index(State(state): State<Arc<AppState>>) -> impl IntoResponse {
    todo!()
}
