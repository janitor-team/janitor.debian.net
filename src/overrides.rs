pub use crate::generated::package_overrides::OverrideConfig;
use protobuf::text_format;
use std::fs::File;
use std::io::Read;

pub fn read_overrides(
    file_path: &std::path::Path,
) -> Result<OverrideConfig, Box<dyn std::error::Error>> {
    let mut file = File::open(file_path)?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;

    Ok(text_format::parse_from_str(&contents)?)
}
