#!/usr/bin/python3

import argparse
import asyncio
import logging
import sys
import traceback
from yarl import URL

from aiohttp import ClientSession
from debian.changelog import Version
from google.protobuf import text_format
from upstream_ontologist.guess import (
    guess_from_aur,
    guess_from_launchpad,
    guess_from_pecl_package,
)
from upstream_ontologist.vcs import guess_repo_from_url

from debian_janitor import package_overrides_pb2


def guess_from_pecl(package):
    if not package.startswith("php-"):
        return iter([])
    php_package = package[4:]
    data = dict(guess_from_pecl_package(php_package.replace("-", "_")))
    try:
        data["Repository"] = guess_repo_from_url(
            data["Repository-Browse"], net_access=True
        )
    except KeyError:
        pass
    return data.items()


async def iter_missing_upstream_branch_packages(base_url):
    async with ClientSession() as session, session.get(
        base_url / "api" / "missing-upstream-urls", raise_for_status=True
    ) as resp:
        for row in await resp.json():
            yield row["package"], Version(row["archive-version"])


async def main(url, start=None):
    async for pkg, version in iter_missing_upstream_branch_packages(URL(url)):
        if start and pkg < start:
            continue
        logging.info("Package: %s" % pkg)
        urls = []
        for name, guesser in [
            ("aur", guess_from_aur),
            ("lp", guess_from_launchpad),
            ("pecl", guess_from_pecl),
        ]:
            try:
                metadata = dict(guesser(pkg))
            except Exception:
                traceback.print_exc()
                continue
            try:
                repo_url = metadata["Repository"]
            except KeyError:
                continue
            else:
                urls.append((name, repo_url))
        if not urls:
            continue
        if len(urls) > 1:
            print("# Note: Conflicting URLs for {}: {!r}".format(pkg, urls))
        config = package_overrides_pb2.OverrideConfig()
        override = config.package.add()
        override.name = pkg
        override.upstream_branch_url = urls[0][1]
        print("# From %s" % urls[0][0])
        text_format.PrintMessage(config, sys.stdout)


parser = argparse.ArgumentParser("guess-upstream-branch-urls")
parser.add_argument(
    "--base-url", type=str, default="https://janitor.debian.net/", help="Instance URL."
)
parser.add_argument(
    "--start",
    type=str,
    default="",
    help="Only process package with names after this one.",
)

args = parser.parse_args()

logging.basicConfig(level=logging.INFO)

asyncio.run(main(args.base_url, args.start))
