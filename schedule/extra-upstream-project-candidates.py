#!/usr/bin/python3

import logging

from breezy import urlutils
from google.protobuf import text_format  # type: ignore

from debian_janitor.candidates_pb2 import CandidateList
from debian_janitor.upstream_project_pb2 import ExtraUpstreamProjects

DEFAULT_VALUE_DEBIANIZE = 50


async def main():
    import argparse

    parser = argparse.ArgumentParser(prog="extra-upstream-project-candidates")
    parser.add_argument(
        "--gcp-logging", action="store_true", help="Use Google cloud logging."
    )

    parser.add_argument(
        "extra_upstream_projects", type=str, help="Path to extra_upstream_projects.conf"
    )

    parser.add_argument("packages", nargs="*", default=None)

    args = parser.parse_args()

    if args.gcp_logging:
        import google.cloud.logging

        client = google.cloud.logging.Client()
        client.get_default_handler()
        client.setup_logging()
    else:
        logging.basicConfig(format="%(message)s", level=logging.INFO)

    with open(args.extra_upstream_projects) as f:
        extra_upstream_config = text_format.Parse(f.read(), ExtraUpstreamProjects())

    for upstream_project in extra_upstream_config.upstream_project:
        cl = CandidateList()
        candidate = cl.candidate.add()
        candidate.distribution = "upstream"
        candidate.package = upstream_project.name
        # TODO(jelmer): Use candidate.maintainer
        # TODO(jelmer): Set context
        # candidate.context = None
        if upstream_project.subpath:
            candidate.command = "debianize %s" % urlutils.join(
                upstream_project.vcs_url, upstream_project.subpath
            )
        else:
            candidate.command = "debianize %s" % upstream_project.vcs_url
        candidate.campaign = "debianize"
        candidate.value = DEFAULT_VALUE_DEBIANIZE
        candidate.origin = args.extra_upstream_projects
        print(cl)


if __name__ == "__main__":
    import asyncio

    asyncio.run(main())
