use chrono::{DateTime, Utc};
use serde::Deserialize;
use std::collections::HashMap;

#[derive(Debug, Deserialize)]
struct ReleaseStages {
    stages: HashMap<String, StageData>,
}

#[derive(Debug, Deserialize)]
struct StageData {
    starts: String,
    // Add other fields as needed
}

#[derive(Debug)]
pub enum Error {
    Io(std::io::Error),
    Reqwest(reqwest::Error),
    SerdeYaml(serde_yaml::Error),
    ChronoParse(chrono::ParseError),
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match &self {
            Error::Io(e) => write!(f, "IO error: {}", e),
            Error::Reqwest(e) => write!(f, "Reqwest error: {}", e),
            Error::SerdeYaml(e) => write!(f, "Serde YAML error: {}", e),
            Error::ChronoParse(e) => write!(f, "Chrono parse error: {}", e),
        }
    }
}

impl std::error::Error for Error {}

pub async fn read_release_stages(url: &str) -> Result<HashMap<String, DateTime<Utc>>, Error> {
    let response = reqwest::get(url).await.map_err(Error::Reqwest)?;
    let body = response.text().await.map_err(Error::Reqwest)?;
    let data: ReleaseStages = serde_yaml::from_str(&body).map_err(Error::SerdeYaml)?;

    let mut ret: HashMap<String, DateTime<Utc>> = HashMap::new();
    for (stage, stage_data) in data.stages {
        if stage_data.starts == "TBA" {
            continue;
        }
        let starts = DateTime::from(
            DateTime::parse_from_rfc3339(&stage_data.starts).map_err(Error::ChronoParse)?,
        );
        ret.insert(stage, starts);
    }

    Ok(ret)
}
