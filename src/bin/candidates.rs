use log::{debug, error, info, warn};
use std::collections::HashMap;
use std::error::Error;
use std::io::Read;

use chrono::Utc;
use clap::Parser;
use lazy_static::lazy_static;
use prometheus::{labels, register_counter, register_gauge, Counter, Gauge};
use protobuf::text_format;
use sqlx::postgres::PgPool;

use debian_janitor::candidates::{Candidate, CandidateList};
use debian_janitor::overrides::{read_overrides, OverrideConfig};
use debian_janitor::packages::Package;
use debian_janitor::policy::read_policy;
use janitor::config::Config;

use debian_janitor::policy::determine_command;

lazy_static! {
    static ref LAST_SUCCESS: Gauge = register_gauge!(
        "job_last_success_unixtime",
        "Last time a batch job successfully finished"
    )
    .unwrap();
    static ref UNKNOWN_CAMPAIGNS_COUNT: Counter =
        register_counter!("unknown_campaigns", "Number of unknown campaigns").unwrap();
    static ref UNKNOWN_CODEBASE_COUNT: Counter =
        register_counter!("unknown_codebases", "Number of unknown codebases").unwrap();
    static ref INVALID_COMMAND_COUNT: Counter = register_counter!(
        "invalid_commands",
        "Number of candidates with invalid command"
    )
    .unwrap();
}

async fn get_package_info(
    pgpool: &PgPool,
) -> Result<HashMap<(String, String), Package>, sqlx::Error> {
    let packages: Vec<Package> = sqlx::query_as("SELECT * FROM package")
        .fetch_all(pgpool)
        .await?;

    Ok(packages
        .into_iter()
        .map(|p| ((p.distribution.clone(), p.name.clone()), p))
        .collect())
}

#[derive(Parser, Debug)]
struct Args {
    #[arg(short, long, value_name = "FILE", default_value = "janitor.conf")]
    config: std::path::PathBuf,

    #[command(flatten)]
    logging: janitor::logging::LoggingArgs,

    /// Promethus pushgateway to send metrics to
    #[arg(long, value_name = "URL", default_value = "http://localhost:9091")]
    prometheus: Option<String>,

    packages: Option<Vec<String>>,

    /// Campaign name
    #[arg(long, value_name = "NAME")]
    campaign: Option<String>,

    /// URL for the runner
    #[arg(long, value_name = "URL", default_value = "http://localhost:8000")]
    runner_url: url::Url,

    /// Path to package overrides
    #[arg(long, value_name = "FILE")]
    package_overrides: Option<String>,

    /// Path to policy file
    #[arg(long, value_name = "FILE", default_value = "policy.conf")]
    policy: String,
}

fn get_candidates_from_script<R: Read>(mut r: R) -> Result<Vec<Candidate>, Box<dyn Error>> {
    let mut buf = String::new();
    r.read_to_string(&mut buf)?;
    Ok(text_format::parse_from_str::<CandidateList>(buf.as_str())?.candidate)
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let args = Args::parse();

    let policy = read_policy(std::path::Path::new(args.policy.as_str()))?;
    let overrides = match args.package_overrides {
        Some(path) => read_overrides(std::path::Path::new(path.as_str()))?,
        None => OverrideConfig::default(),
    };

    let release_stages_passed = match &policy.freeze_dates_url {
        Some(url) => Some(debian_janitor::release::read_release_stages(url).await?),
        None => None,
    };

    args.logging.init();

    let _ret = 0;
    let mut errors = 0;
    let mut total = 0;

    use janitor::config::read_file;
    let config = read_file(args.config.as_path()).unwrap();
    let pool = if let Some(db_location) = config.database_location.as_ref() {
        PgPool::connect(db_location).await
    } else {
        sqlx::postgres::PgPool::connect_with(sqlx::postgres::PgConnectOptions::new()).await
    }
    .unwrap();

    let package_info = get_package_info(&pool).await?;

    let client = reqwest::Client::new();

    let candidates = get_candidates_from_script(std::io::stdin())?;

    for candidate in candidates {
        let campaign = candidate
            .campaign
            .as_deref()
            .unwrap_or_else(|| args.campaign.as_deref().unwrap());
        let info = package_info.get(&(
            candidate.distribution.clone().unwrap(),
            candidate.package.clone().unwrap(),
        ));
        let codebase: String = if let Some(info) = info {
            if candidate.distribution.as_deref() == Some("sid") {
                info.codebase.clone().unwrap()
            } else {
                format!(
                    "{}-{}",
                    info.codebase.as_deref().unwrap(),
                    candidate.distribution.unwrap()
                )
            }
        } else if candidate.distribution.as_deref() == Some("upstream") {
            ensure_upstream_package_exists(
                &pool,
                &config,
                candidate.package.as_deref().unwrap(),
                Some(candidate.origin.unwrap_or(campaign.to_string())),
            )
            .await?
        } else {
            warn!(
                "Unknown package {} in distribution {}",
                candidate.package.as_deref().unwrap(),
                candidate.distribution.unwrap()
            );
            continue;
        };

        let command = candidate.command.unwrap_or_else(|| {
            determine_command(
                &policy,
                &overrides,
                &config,
                info,
                release_stages_passed.as_ref(),
                candidate.campaign.as_deref().unwrap(),
                candidate.extra_arg.as_slice(),
            )
            .unwrap()
        });
        let json_candidate = serde_json::json!({
            "codebase": codebase,
            "campaign": candidate.campaign,
            "publish-policy": format!("{}-{}", candidate.campaign.unwrap(), candidate.package.unwrap()),
            "command": command,
            "context": candidate.context,
            "value": candidate.value,
            "success_chance": candidate.success_chance,
            "requester": args.campaign.as_deref(),
            "comment": candidate.comment,
        });

        total += 1;
        debug!("Uploading candidate: {:?}", json_candidate);

        match upload_candidate(&client, &args.runner_url, &json_candidate).await {
            Ok(_result) => {}
            Err(e) => {
                warn!("Error uploading candidate: {}", e);
                errors += 1;
            }
        };
    }
    info!("Uploaded {} candidates, {} errors", total, errors);

    LAST_SUCCESS.set(Utc::now().timestamp() as f64);

    if args.prometheus.is_some() && args.campaign.is_some() {
        let push_result = prometheus::push_metrics(
            "debian_janitor.candidates",
            labels! {
                "campaign".to_string() => args.campaign.as_ref().unwrap().clone(),
            },
            args.prometheus.unwrap().as_str(),
            prometheus::gather(),
            None,
        );
        match push_result {
            Ok(()) => {}
            Err(e) => {
                error!("Prometheus push error: {:?}", e);
            }
        }
    }

    info!("Uploaded {} candidates with {} errors", total, errors);
    Ok(())
}

async fn ensure_upstream_package_exists(
    conn: &PgPool,
    config: &Config,
    package: &str,
    origin: Option<String>,
) -> Result<String, Box<dyn Error>> {
    let codebase = format!("{}-upstream", package);

    sqlx::query(
        r#"INSERT INTO codebase (name, branch_url, url, branch, subpath) VALUES ($1, $2, $3, $4, $5) ON CONFLICT DO NOTHING"#,
    )
    .bind(&codebase)
    .bind(&None::<String>)
    .bind(&None::<String>)
    .bind(&None::<String>)
    .bind(&None::<String>)
    .execute(conn)
    .await?;

    sqlx::query(
        "INSERT INTO package (name, distribution, branch_url, subpath, \
        maintainer_email, origin, vcs_url, codebase) \
        VALUES ($1, $2, $3, $4, $5, $6, $7, $8) ON CONFLICT DO NOTHING",
    )
    .bind(package)
    .bind("upstream")
    .bind(&None::<String>)
    .bind("")
    .bind(&config.committer)
    .bind(&origin)
    .bind(&None::<String>)
    .bind(&codebase)
    .execute(conn)
    .await?;

    Ok(codebase)
}

async fn upload_candidate(
    client: &reqwest::Client,
    runner_url: &url::Url,
    json_candidate: &serde_json::Value,
) -> Result<(), Box<dyn Error>> {
    let url = format!("{}/candidates", runner_url);
    let response = client
        .post(&url)
        .header("Content-Type", "application/json")
        .body(json_candidate.to_string())
        .send()
        .await?
        .error_for_status()?;

    let result = response.json::<serde_json::Value>().await?;

    if let Some(unknown_campaigns) = result.get("unknown_campaigns") {
        warn!("unknown campaigns: {:?}", unknown_campaigns);
        UNKNOWN_CAMPAIGNS_COUNT.inc();
    }

    if let Some(unknown_codebases) = result.get("unknown_codebases") {
        warn!("unknown codebases: {:?}", unknown_codebases);
        UNKNOWN_CODEBASE_COUNT.inc();
    }

    if let Some(invalid_command) = result.get("invalid_command") {
        warn!("invalid command: {:?}", invalid_command);
        INVALID_COMMAND_COUNT.inc();
    }

    Ok(())
}
