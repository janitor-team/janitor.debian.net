use axum::{
    response::IntoResponse,
    routing::Router,
    routing::{get, post},
};
use clap::Parser;
use std::path::PathBuf;
use std::sync::Arc;
use url::Url;

use debian_janitor::policy::PolicyApplier;

#[derive(Parser)]
struct Args {
    #[clap(long, default_value = "janitor.conf")]
    /// Path to configuration
    config: PathBuf,

    #[clap(long)]
    subscribe: bool,

    #[clap(long, short, conflicts_with = "subscribe")]
    /// Run ID to process
    run_id: Vec<String>,

    #[clap(long)]
    /// Enable debug logging
    debug: bool,

    #[clap(long)]
    /// URL for ognibuild dep server
    dep_server_url: Option<String>,

    #[clap(flatten)]
    logging: janitor::logging::LoggingArgs,

    #[clap(long, default_value = "localhost")]
    /// Listen address
    listen_address: String,

    #[clap(long, default_value = "9931")]
    /// Listen port
    port: u16,

    #[clap(long, default_value = "policy.conf")]
    /// Path to policy file
    policy: PathBuf,

    #[clap(long)]
    /// URL for publisher
    publisher_url: Option<Url>,

    #[clap(long)]
    /// Reprocess already processed runs
    reprocess: bool,

    #[clap(long)]
    /// Path to package overrides
    package_overrides: Option<PathBuf>,
}

#[derive(Debug)]
struct AppState {
    publisher_url: Option<Url>,

    config: janitor::config::Config,

    pool: sqlx::PgPool,

    policy_applier: debian_janitor::policy::PolicyApplier,
}

pub const DEFAULT_SUCCESS_CHANCE: f64 = 0.5;

async fn handle_ready() -> impl IntoResponse {
    "ok"
}

async fn handle_health() -> impl IntoResponse {
    "ok"
}

#[tokio::main]
async fn main() -> Result<(), i32> {
    let args = Args::parse();

    args.logging.init();

    let config = janitor::config::read_file(&args.config).map_err(|e| {
        eprintln!("Failed to read configuration: {}", e);
        1
    })?;

    // TODO(jelmer): Use schroot rather than PlainSession
    let sid_session: Box<dyn ognibuild::session::Session> =
        Box::new(ognibuild::session::plain::PlainSession::new());

    let mut policy_applier =
        PolicyApplier::from_paths(&args.policy, args.package_overrides.as_deref()).map_err(
            |e| {
                eprintln!("Failed to load policy: {}", e);
                1
            },
        )?;
    policy_applier.load_release_stages().await.unwrap();
    let apt_mgr = ognibuild::debian::apt::AptManager::from_session(sid_session.as_ref());

    let pool = janitor::state::create_pool(&config).await.map_err(|e| {
        eprintln!("Failed to create database pool: {}", e);
        1
    })?;

    let state = AppState {
        publisher_url: args.publisher_url,
        config,
        pool,
        policy_applier,
    };

    let router = Router::new()
        .route("/ready", get(handle_ready))
        .route("/health", get(handle_health))
        .with_state(Arc::new(state));

    let tcp_listener = tokio::net::TcpListener::bind((args.listen_address.as_str(), args.port))
        .await
        .map_err(|e| {
            eprintln!("Failed to bind to address: {}", e);
            1
        })?;

    axum::serve(tcp_listener, router.into_make_service())
        .await
        .unwrap();

    Ok(())
}
