use clap::Parser;

use debian_janitor::candidates::Candidate;

use sqlx::postgres::PgPool;
use sqlx::Arguments;
use std::error::Error;

#[derive(Parser, Debug)]
struct Args {
    #[arg(short, long, value_name = "FILE")]
    config: Option<std::path::PathBuf>,

    #[command(flatten)]
    udd: debian_janitor::udd::UDDArgs,

    #[command(flatten)]
    logging: janitor::logging::LoggingArgs,

    packages: Option<Vec<String>>,
}

const DEFAULT_VALUE_NEW_UPSTREAM_SNAPSHOTS: i32 = 20;

#[derive(sqlx::FromRow, Debug)]
struct FreshSnapshotRow {
    source: String,
    upstream_repo_known: bool,
}

pub async fn iter_fresh_snapshots_candidates(
    pool: &PgPool,
    release: &str,
    packages: Option<Vec<&str>>,
) -> Result<(), Box<dyn Error>> {
    let mut args = sqlx::postgres::PgArguments::default();
    args.add(&release);

    let mut query = "
        SELECT DISTINCT ON (sources.source)
            sources.source,
            EXISTS (
                SELECT 1
                FROM upstream_metadata
                WHERE key = 'Repository' AND source = sources.source
            ) AS upstream_repo_known
        FROM
            sources
        WHERE
            sources.vcs_url != '' AND
            POSITION('-' IN sources.version) > 0 AND
            sources.release = $1
    "
    .to_owned();

    if let Some(packages) = packages {
        query += " AND sources.source = ANY($1::text[])";
        args.add(&packages);
    }

    query += " ORDER BY sources.source, sources.version DESC";

    let rows: Vec<FreshSnapshotRow> = sqlx::query_as_with(&query, args)
        .fetch_all(pool)
        .await
        .expect("Failed to fetch candidates");

    for row in rows {
        let candidate = Candidate {
            distribution: Some(release.to_owned()),
            package: Some(row.source),
            campaign: Some("fresh-snapshots".to_owned()),
            value: Some(DEFAULT_VALUE_NEW_UPSTREAM_SNAPSHOTS),
            success_chance: Some(if row.upstream_repo_known { 1.0 } else { 0.1 }),
            ..Default::default()
        };

        debian_janitor::candidates::print_candidate_as_textpb(candidate);
    }

    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let args = Args::parse();

    args.logging.init();

    let pool = args.udd.connect().await?;

    let mut tags = std::collections::HashSet::new();
    for fixer in lintian_brush::available_lintian_fixers(None, None)? {
        tags.extend(fixer.lintian_tags().into_iter());
    }

    iter_fresh_snapshots_candidates(
        &pool,
        "sid",
        args.packages
            .as_ref()
            .map(|ps| ps.iter().map(|s| s.as_str()).collect()),
    )
    .await?;

    Ok(())
}
