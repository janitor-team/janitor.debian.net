use clap::Parser;

use debian_janitor::candidates::Candidate;
use std::error::Error;

#[derive(Parser, Debug)]
struct Args {
    #[command(flatten)]
    logging: janitor::logging::LoggingArgs,

    #[arg(short, long, value_name = "PACKAGE", default_value = "sid")]
    release: String,

    packages: Option<Vec<String>>,
}

fn iter_multiarch_candidates(
    release: &str,
    packages: Option<Vec<&str>>,
) -> Result<(), Box<dyn std::error::Error>> {
    let hints_text = multiarch_hints::download_multiarch_hints(None, None)?.unwrap();
    println!("Downloading multiarch hints for {}", release);
    let hints = multiarch_hints::parse_multiarch_hints(hints_text.as_slice())?;

    for (source, hints) in multiarch_hints::multiarch_hints_by_source(&hints) {
        if let Some(packages) = &packages {
            if !packages.contains(&source) {
                continue;
            }
        }

        let hints = hints
            .iter()
            .map(|h| h.kind().to_string())
            .collect::<Vec<_>>();

        let value = multiarch_hints::calculate_value(hints.iter().map(|h| h.parse().unwrap()).collect().as_slice());
        let candidate = Candidate {
            distribution: Some(release.to_string()),
            campaign: Some("multiarch-fixes".to_string()),
            package: Some(source.to_string()),
            value: Some(value),
            context: Some(hints.join(" ")),
            ..Default::default()
        };

        debian_janitor::candidates::print_candidate_as_textpb(candidate);
    }
    Ok(())
}

fn main() -> Result<(), Box<dyn Error>> {
    let args = Args::parse();

    args.logging.init();

    iter_multiarch_candidates(
        args.release.as_str(),
        args.packages
            .as_ref()
            .map(|ps| ps.iter().map(|s| s.as_str()).collect()),
    )?;

    Ok(())
}
