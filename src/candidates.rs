pub use crate::generated::candidates::{Candidate, CandidateList};
use protobuf::text_format;

pub fn print_candidate_as_textpb(candidate: Candidate) {
    let cl = CandidateList {
        candidate: vec![candidate],
        ..Default::default()
    };

    let mut buf = String::new();
    text_format::print_to(&cl, &mut buf);

    println!("{}", buf);
}
