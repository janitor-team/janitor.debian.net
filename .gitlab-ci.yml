---
variables:
  POSTGRES_VERSION: "16"

build-site:
  stage: build
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - mkdir -p /kaniko/.docker
    - echo "{\"auths\":{\"${CI_REGISTRY}\":{\"auth\":\"$(printf "%s:%s" "${CI_REGISTRY_USER}" "${CI_REGISTRY_PASSWORD}" | base64 | tr -d '\n')\"}}}" > /kaniko/.docker/config.json
    - >-
      /kaniko/executor
      --context "${CI_PROJECT_DIR}"
      --dockerfile "${CI_PROJECT_DIR}/Dockerfile_site"
      --destination "${CI_REGISTRY_IMAGE}/site:${CI_COMMIT_SHA}"
      --destination "${CI_REGISTRY_IMAGE}/site:${CI_COMMIT_BRANCH}"
      --destination "${CI_REGISTRY_IMAGE}/site:latest"
  rules:
    - if: "$CI_COMMIT_BRANCH == $CI_MASTER_BRANCH"

build-followup:
  stage: build
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - mkdir -p /kaniko/.docker
    - echo "{\"auths\":{\"${CI_REGISTRY}\":{\"auth\":\"$(printf "%s:%s" "${CI_REGISTRY_USER}" "${CI_REGISTRY_PASSWORD}" | base64 | tr -d '\n')\"}}}" > /kaniko/.docker/config.json
    - >-
      /kaniko/executor
      --context "${CI_PROJECT_DIR}"
      --dockerfile "${CI_PROJECT_DIR}/Dockerfile_followup"
      --destination "${CI_REGISTRY_IMAGE}/followup:${CI_COMMIT_SHA}"
      --destination "${CI_REGISTRY_IMAGE}/followup:${CI_COMMIT_BRANCH}"
      --destination "${CI_REGISTRY_IMAGE}/followup:latest"
  rules:
    - if: "$CI_COMMIT_BRANCH == $CI_MASTER_BRANCH"

build-schedule:
  stage: build
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - mkdir -p /kaniko/.docker
    - echo "{\"auths\":{\"${CI_REGISTRY}\":{\"auth\":\"$(printf "%s:%s" "${CI_REGISTRY_USER}" "${CI_REGISTRY_PASSWORD}" | base64 | tr -d '\n')\"}}}" > /kaniko/.docker/config.json
    - >-
      /kaniko/executor
      --context "${CI_PROJECT_DIR}"
      --dockerfile "${CI_PROJECT_DIR}/Dockerfile_schedule"
      --destination "${CI_REGISTRY_IMAGE}/schedule:${CI_COMMIT_SHA}"
      --destination "${CI_REGISTRY_IMAGE}/schedule:${CI_COMMIT_BRANCH}"
      --destination "${CI_REGISTRY_IMAGE}/schedule:latest"
  rules:
    - if: "$CI_COMMIT_BRANCH == $CI_MASTER_BRANCH"

build-worker:
  stage: build
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - mkdir -p /kaniko/.docker
    - echo "{\"auths\":{\"${CI_REGISTRY}\":{\"auth\":\"$(printf "%s:%s" "${CI_REGISTRY_USER}" "${CI_REGISTRY_PASSWORD}" | base64 | tr -d '\n')\"}}}" > /kaniko/.docker/config.json
    - >-
      /kaniko/executor
      --context "${CI_PROJECT_DIR}"
      --dockerfile "${CI_PROJECT_DIR}/Dockerfile_worker"
      --destination "${CI_REGISTRY_IMAGE}/worker:${CI_COMMIT_SHA}"
      --destination "${CI_REGISTRY_IMAGE}/worker:${CI_COMMIT_BRANCH}"
      --destination "${CI_REGISTRY_IMAGE}/worker:latest"
  rules:
    - if: "$CI_COMMIT_BRANCH == $CI_MASTER_BRANCH"

build-matrix-notify:
  stage: build
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - mkdir -p /kaniko/.docker
    - echo "{\"auths\":{\"${CI_REGISTRY}\":{\"auth\":\"$(printf "%s:%s" "${CI_REGISTRY_USER}" "${CI_REGISTRY_PASSWORD}" | base64 | tr -d '\n')\"}}}" > /kaniko/.docker/config.json
    - >-
      /kaniko/executor
      --context "${CI_PROJECT_DIR}/matrix-notify"
      --destination "${CI_REGISTRY_IMAGE}/matrix-notify:${CI_COMMIT_SHA}"
      --destination "${CI_REGISTRY_IMAGE}/matrix-notify:${CI_COMMIT_BRANCH}"
      --destination "${CI_REGISTRY_IMAGE}/matrix-notify:latest"
  rules:
    - if: "$CI_COMMIT_BRANCH == $CI_MASTER_BRANCH"

build-mastodon-notify:
  stage: build
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - mkdir -p /kaniko/.docker
    - echo "{\"auths\":{\"${CI_REGISTRY}\":{\"auth\":\"$(printf "%s:%s" "${CI_REGISTRY_USER}" "${CI_REGISTRY_PASSWORD}" | base64 | tr -d '\n')\"}}}" > /kaniko/.docker/config.json
    - >-
      /kaniko/executor
      --context "${CI_PROJECT_DIR}/mastodon-notify"
      --destination "${CI_REGISTRY_IMAGE}/mastodon-notify:${CI_COMMIT_SHA}"
      --destination "${CI_REGISTRY_IMAGE}/mastodon-notify:${CI_COMMIT_BRANCH}"
      --destination "${CI_REGISTRY_IMAGE}/mastodon-notify:latest"
  rules:
    - if: "$CI_COMMIT_BRANCH == $CI_MASTER_BRANCH"

test-lint:
  stage: test
  image: debian:sid

  before_script:
    - apt -y update
    - apt -y install python3-pip make protobuf-compiler git libpcre3-dev python3-debianbts rustc libpq-dev python3-gpg python3-apt libapt-pkg-dev libjs-chart.js libjs-jquery libjs-moment libjs-jquery-datatables libjs-jquery-typeahead postgresql-${POSTGRES_VERSION} postgresql-${POSTGRES_VERSION}-debversion sudo codespell libssl-dev pkg-config libtdb-dev libclang-dev python3-setuptools-rust
    - pip3 install --upgrade pip --break-system-packages
    - python3 -m pip install --break-system-packages "setuptools-protobuf[mypy]"
    - python3 -m pip install --break-system-packages ".[dev,gcp]"

  script:
    - make lint
    - make typing
    - codespell
    # postgres doesn't like running as root..
    - useradd -d /builds/{GITLAB_USER} -g users -M -N builder
    - chown -R builder:users ..
    - sudo -H -i -u builder make test
