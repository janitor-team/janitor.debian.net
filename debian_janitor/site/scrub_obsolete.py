import aiozipkin
from janitor.site.common import html_template

CAMPAIGN = "scrub-obsolete"


@html_template("scrub-obsolete/start.html")
async def handle_scrub_obsolete_start(request):
    return {}


@html_template("scrub-obsolete/package.html", headers={"Cache-Control": "max-age=600"})
async def handle_scrub_obsolete_pkg(request):
    from debian_janitor.site import generate_pkg_context

    # TODO(jelmer): Handle Accept: text/diff
    pkg = request.match_info["pkg"]
    run_id = request.match_info.get("run_id")
    return await generate_pkg_context(
        request.app.database,
        request.app["config"],
        "scrub-obsolete",
        request.app["http_client_session"],
        request.app["differ_url"],
        request.app["vcs_managers"],
        pkg,
        aiozipkin.request_span(request),
        run_id,
    )


def register_scrub_obsolete_endpoints(router):
    router.add_get(
        "/scrub-obsolete/", handle_scrub_obsolete_start, name="scrub-obsolete-start"
    )
    router.add_get(
        "/scrub-obsolete/pkg/{pkg}/",
        handle_scrub_obsolete_pkg,
        name="scrub-obsolete-package",
    )
    router.add_get(
        "/scrub-obsolete/pkg/{pkg}/{run_id}",
        handle_scrub_obsolete_pkg,
        name="scrub-obsolete-package-run",
    )
