use debian_janitor::repology::{Project};
use log::warn;

fn iter_repo<'a>(
    repos: &'a [&'a Project],
    repo: &'a str,
) -> impl Iterator<Item = &'a Project> + 'a {
    repos
        .iter()
        .filter_map(move |p| if p.repo == repo { Some(*p) } else { None })
}

fn repology_to_debian_srcname(name: &str, repos: &[&Project]) -> Option<String> {
    if !name.contains(':') {
        return Some(name.to_owned());
    }

    let mut parts = name.split(':');
    let family = parts.next().unwrap();
    let name = parts.next().unwrap();

    match family {
        "python" => Some(format!("python-{}", name)),
        "perl" => Some(format!("lib{}-perl", name)),
        "rust" => Some(format!("rust-{}", name)),
        "ruby" => Some(format!("ruby-{}", name)),
        "r" => {
            if let Some(cran_package) = iter_repo(repos, "cran").next() {
                Some(format!("r-cran-{}", cran_package.srcname.as_ref().unwrap()))
            } else {
                warn!("unable to find R repository for r package {}", name);
                None
            }
        }
        "gnome" => Some(format!("gnome-{}", name)),
        "go" => Some(format!("golang-{}", name)),
        "haskell" => Some(format!("haskell-{}", name)),
        "octave" => Some(format!("octave-{}", name)),
        "node" => Some(format!("node-{}", name)),
        "php" => Some(format!("php-{}", name)),
        "purple" => Some(format!("purple-{}", name)),
        _ => {
            warn!("unsupported repology family {:?} in {}", family, name);
            None
        }
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_repology_to_debian_srcname() {
        use super::repology_to_debian_srcname;
        use debian_janitor::repology::{Project, Status};

        let foo_project = Project {
            repo: "cran".to_owned(),
            srcname: Some("foo".to_owned()),
            visiblename: "foo".to_owned(),
            version: "1.0.0".to_owned(),
            status: Status::Newest,
            origversion: None,
            maintainers: None,
            summary: None,
            binname: None,
            licenses: None,
        };
        let bar_project = Project {
            repo: "cran".to_owned(),
            srcname: Some("bar".to_owned()),
            visiblename: "r:bar".to_owned(),
            version: "1.0.0".to_owned(),
            status: Status::Newest,
            origversion: None,
            maintainers: None,
            summary: None,
            binname: None,
            licenses: None,
        };

        assert_eq!(
            repology_to_debian_srcname("r:foo", &[&foo_project]),
            Some("r-cran-foo".to_owned())
        );
        assert_eq!(
            repology_to_debian_srcname("r:bar", &[&bar_project]),
            Some("r-cran-bar".to_owned())
        );
        assert_eq!(
            repology_to_debian_srcname("r:baz", &[&foo_project]),
            Some("r-cran-foo".to_owned())
        );
        assert_eq!(
            repology_to_debian_srcname("r:qux", &[&foo_project]),
            Some("r-cran-foo".to_owned())
        );
    }
}

fn main() {
    unimplemented!();
}
