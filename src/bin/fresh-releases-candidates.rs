use clap::Parser;

use debian_janitor::candidates::Candidate;

use semver::Version;
use sqlx::postgres::{PgPool};
use sqlx::Arguments;
use std::error::Error;

const DEFAULT_VALUE_NEW_UPSTREAM: i32 = 30;
const INVALID_VERSION_DOWNGRADE: i32 = 5;

#[derive(Parser, Debug)]
struct Args {
    #[arg(short, long, value_name = "FILE")]
    config: Option<std::path::PathBuf>,

    #[command(flatten)]
    udd: debian_janitor::udd::UDDArgs,

    #[command(flatten)]
    logging: janitor::logging::LoggingArgs,

    packages: Option<Vec<String>>,
}

#[derive(sqlx::FromRow, Debug)]
struct UpstreamVersionRow {
    source: String,
    upstream_version: Option<String>,
}

pub async fn iter_fresh_releases_candidates(
    pool: &PgPool,
    distribution: &str,
    packages: Option<Vec<&str>>,
) -> Result<(), Box<dyn Error>> {
    let mut args = sqlx::postgres::PgArguments::default();
    args.add(distribution);

    let mut query = "
        SELECT DISTINCT ON (sources.source)
            sources.source,
            upstream.upstream_version
        FROM
            upstream
            INNER JOIN sources ON upstream.version = sources.version AND upstream.source = sources.source
        WHERE
            status = 'newer package available' AND
            sources.vcs_url != '' AND
            sources.release = $1
    ".to_owned();

    if let Some(packages) = packages {
        query += " AND upstream.source = ANY($2::text[])";
        args.add(&packages);
    }

    query += " ORDER BY sources.source, sources.version DESC";

    let rows: Vec<UpstreamVersionRow> = sqlx::query_as_with(&query, args)
        .fetch_all(pool)
        .await
        .expect("Failed to fetch candidates");

    for row in rows {
        let mut value = DEFAULT_VALUE_NEW_UPSTREAM;
        if let Ok(_version) = Version::parse(row.upstream_version.as_ref().unwrap()) {
            // Version parsed successfully, subtract the invalid downgrade value
            value -= INVALID_VERSION_DOWNGRADE;
        }
        let candidate = Candidate {
            distribution: Some(distribution.to_owned()),
            package: Some(row.source),
            context: row.upstream_version,
            campaign: Some("fresh-releases".to_owned()),
            value: Some(value),
            ..Default::default()
        };

        debian_janitor::candidates::print_candidate_as_textpb(candidate);
    }
    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let args = Args::parse();

    args.logging.init();

    let pool = args.udd.connect().await?;

    let mut tags = std::collections::HashSet::new();
    for fixer in lintian_brush::available_lintian_fixers(None, None)? {
        tags.extend(fixer.lintian_tags().into_iter());
    }

    iter_fresh_releases_candidates(
        &pool,
        "sid",
        args.packages
            .as_ref()
            .map(|ps| ps.iter().map(|s| s.as_str()).collect()),
    )
    .await?;

    Ok(())
}
