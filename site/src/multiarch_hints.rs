use crate::AppState;
use axum::Router;
use std::sync::Arc;

pub fn routes(state: Arc<AppState>) -> Router<Arc<AppState>> {
    Router::new().with_state(state)
}
