use axum::{response::IntoResponse, routing::get, Router};
use clap::Parser;
use futures::StreamExt;
use matrix_sdk::{
    room::Room, ruma::events::room::message::RoomMessageEventContent, Client as MatrixClient,
};
use std::sync::Arc;
use tokio::sync::Mutex;

#[derive(Parser)]
struct Args {
    #[arg(long, default_value = "")]
    password: String,
    #[arg(long)]
    homeserver_url: url::Url,
    #[arg(long, default_value = "")]
    user: String,
    #[arg(long, default_value = "")]
    redis_url: String,
    #[arg(long, default_value = "https://janitor.debian.net/")]
    janitor_url: url::Url,
    #[arg(long, default_value = "")]
    room: String,
    #[arg(long, default_value = "127.0.0.1")]
    prometheus_listen_address: std::net::IpAddr,
    #[arg(long, default_value = "9918")]
    prometheus_port: u16,

    #[clap(flatten)]
    logging: janitor::logging::LoggingArgs,
}

fn lintian_fixes_suffix(
    d: &janitor::publish::PublishNotification,
    janitor_url: &url::Url,
) -> (String, String) {
    let result: lintian_brush::ManyResult = serde_json::from_value(d.result.clone()).unwrap();

    let tags = result
        .success
        .iter()
        .flat_map(|(entry, _)| entry.fixed_lintian_tags())
        .map(|x| x.to_string())
        .collect::<std::collections::HashSet<_>>();

    // sort tags
    let mut tags = tags.into_iter().collect::<Vec<_>>();
    tags.sort();

    if !tags.is_empty() {
        let out = format!("fixing: {}.", tags.join(", "));
        let outhtml = format!(
            "fixing: {}.",
            tags.iter()
                .map(|tag| {
                    format!(r#"<a href="{}/tags/{}.html">{}</a>"#, janitor_url, tag, tag)
                })
                .collect::<Vec<_>>()
                .join(", ")
        );
        (out, outhtml)
    } else {
        (String::new(), String::new())
    }
}

async fn handle_publish(
    d: &janitor::publish::PublishNotification,
    room: &Room,
    janitor_url: &url::Url,
) {
    if d.mode != janitor::publish::Mode::Push || d.result_code != "success" {
        return;
    }
    let url = d
        .main_branch_web_url
        .as_ref()
        .or(d.main_branch_url.as_ref());
    let (mut out, mut outhtml) = if let Some(url) = url {
        let out = format!("Pushed {} changes to {} ({})", d.campaign, url, d.codebase);
        let outhtml = format!(
            "Pushed <a href=\"{}/{}\">{}</a> changes to <a href=\"{}\">{}</a>",
            janitor_url,
            d.campaign.clone(),
            d.campaign,
            url,
            d.codebase
        );
        (out, outhtml)
    } else {
        let out = format!("Pushed {} changes to {}", d.campaign, d.codebase);
        let outhtml = format!(
            "Pushed <a href=\"{}\">{}</a> changes to {}",
            janitor_url.join(&d.campaign).unwrap(),
            d.campaign,
            d.codebase
        );
        (out, outhtml)
    };
    #[allow(clippy::single_match)]
    match d.campaign.as_str() {
        "lintian-fixes" => {
            let (extra_out, extra_outhtml) = lintian_fixes_suffix(d, janitor_url);
            out.push_str(&extra_out);
            outhtml.push_str(&extra_outhtml);
        }
        _ => {}
    }
    let content = RoomMessageEventContent::text_html(out, outhtml);
    room.send(content).await.unwrap();
}

async fn handle_merge_proposal(
    d: &janitor::publish::MergeProposalNotification,
    room: &Room,
    janitor_url: &url::Url,
) {
    if d.status != janitor::publish::MergeProposalStatus::Merged {
        return;
    }
    let out = format!(
        "Merge proposal {} ({}/{}) merged{}.",
        d.url,
        d.codebase,
        d.campaign,
        if let Some(merged_by) = &d.merged_by {
            format!(" by {}", merged_by)
        } else {
            String::new()
        }
    );
    let outhtml = format!(
        r#"Merge proposal <a href="{}">{}</a> for <a href="{}">{}</a>/{}
        merged{}."#,
        d.url,
        d.url,
        janitor_url.join(&d.campaign).unwrap(),
        d.campaign,
        d.codebase,
        if let Some(merged_by) = &d.merged_by {
            if let Some(merged_by_url) = &d.merged_by_url {
                format!(r#" by <a href="{}">{}</a>"#, merged_by_url, merged_by)
            } else {
                format!(" by {}", merged_by)
            }
        } else {
            String::new()
        }
    );
    let content = RoomMessageEventContent::text_html(out, outhtml);
    room.send(content).await.unwrap();
}

async fn health_check() -> impl IntoResponse {
    "ok"
}

#[tokio::main]
async fn main() {
    let args = Args::parse();

    args.logging.init();

    let matrix_client = MatrixClient::new(args.homeserver_url).await.unwrap();
    matrix_client
        .matrix_auth()
        .login_username(&args.user, &args.password)
        .initial_device_display_name("debian-janitor notifications")
        .await
        .unwrap();

    let (room, server) = args.room.split_at(args.room.find(':').unwrap());

    let room = matrix_client
        .join_room_by_id_or_alias(
            room.try_into().unwrap(),
            &[matrix_sdk::OwnedServerName::try_from(server).unwrap()],
        )
        .await
        .unwrap();
    let matrix_client = Arc::new(Mutex::new(matrix_client));

    // Setup HTTP server for Prometheus metrics
    let app = Router::new().route("/health", get(health_check));

    let addr = std::net::SocketAddr::new(args.prometheus_listen_address, args.prometheus_port);
    let listener = tokio::net::TcpListener::bind(&addr).await.unwrap();
    tokio::spawn(async move {
        axum::serve(listener, app.into_make_service())
            .await
            .unwrap();
    });

    // Redis connection and pub/sub
    let janitor_url = args.janitor_url.clone();
    let publish_room = room.clone();
    let redis_url = args.redis_url.clone();
    // Subscribe to the publish channel
    tokio::spawn(async move {
        let client = redis::Client::open(redis_url).unwrap();

        let mut pubsub = client.get_async_pubsub().await.unwrap();

        pubsub.subscribe("publish").await.unwrap();

        let mut stream = pubsub.into_on_message();

        while let Some(msg) = stream.next().await {
            let msg: String = msg.get_payload().unwrap();
            let publish_result: janitor::publish::PublishNotification =
                serde_json::from_str(&msg).unwrap();
            handle_publish(&publish_result, &publish_room, &janitor_url).await;
        }
    });

    let janitor_url = args.janitor_url.clone();
    let mp_room = room.clone();
    let redis_url = args.redis_url.clone();
    // Subscribe to the merge-proposal channel
    tokio::spawn(async move {
        let client = redis::Client::open(redis_url).unwrap();
        let mut pubsub = client.get_async_pubsub().await.unwrap();
        pubsub.subscribe("merge-proposal").await.unwrap();

        let mut stream = pubsub.on_message();

        while let Some(msg) = stream.next().await {
            let msg: String = msg.get_payload().unwrap();
            let merge_proposal: janitor::publish::MergeProposalNotification =
                serde_json::from_str(&msg).unwrap();
            handle_merge_proposal(&merge_proposal, &mp_room, &janitor_url).await;
        }
    });

    let sync_settings = matrix_sdk::config::SyncSettings::new();

    // Sync the Matrix client
    matrix_client
        .lock()
        .await
        .sync(sync_settings)
        .await
        .unwrap();
}
