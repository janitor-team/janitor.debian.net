import os

from janitor.publish_one import load_template_env

env = load_template_env(
    os.path.join(os.path.dirname(__file__), "../k8s/proposal-templates")
)
env.globals["external_url"] = "https://www.example.com"


DEBDIFF = """\
[The following lists of changes regard files as different if they have
different names, permissions or owners.]

Files in second .changes but not in first
-----------------------------------------
-rw-r--r--  root/root   /usr/lib/debug/.build-id/e4/3520e0f1e.debug

Files in first .changes but not in second
-----------------------------------------
-rw-r--r--  root/root   /usr/lib/debug/.build-id/28/0303571bd.debug

Control files of package xserver-blah: lines which differ (wdiff format)
------------------------------------------------------------------------
Installed-Size: [-174-] {+170+}
Version: [-1:1.7.9-2~jan+unchanged1-] {+1:1.7.9-3~jan+lint1+}

Control files of package xserver-dbgsym: lines which differ (wdiff format)
--------------------------------------------------------------------------
Build-Ids: [-280303571bd7f8-] {+e43520e0f1eb+}
Depends: xserver-blah (= [-1:1.7.9-2~jan+unchanged1)-] {+1:1.7.9-3~jan+lint1)+}
Installed-Size: [-515-] {+204+}
Version: [-1:1.7.9-2~jan+unchanged1-] {+1:1.7.9-3~jan+lint1+}
"""


def test_binary_diff():
    assert """
These changes have no impact on the [binary debdiff](https://www.example.com/api/run/some-log-id/debdiff?filter_boring=1).

You can also view the [diffoscope diff](https://www.example.com/api/run/some-log-id/diffoscope?filter_boring=1) ([unfiltered](https://www.example.com/api/run/some-log-id/diffoscope)).
""" == env.get_template("binary-diff.md").render(
        {"role": "main", "log_id": "some-log-id", "debdiff": "FOO\n" * 50}
    )

    assert """
## Debdiff

These changes affect the binary packages:

[The following lists of changes regard files as different if they have
different names, permissions or owners.]
### Files in second .changes but not in first
    -rw-r--r--  root/root   /usr/lib/debug/.build-id/e4/3520e0f1e.debug
### Files in first .changes but not in second
    -rw-r--r--  root/root   /usr/lib/debug/.build-id/28/0303571bd.debug
### Control files of package xserver-blah: lines which differ (wdiff format)
* Installed-Size: [-174-] {+170+}
* Version: [-1:1.7.9-2~jan+unchanged1-] {+1:1.7.9-3~jan+lint1+}
### Control files of package xserver-dbgsym: lines which differ (wdiff format)
* Build-Ids: [-280303571bd7f8-] {+e43520e0f1eb+}
* Depends: xserver-blah (= [-1:1.7.9-2~jan+unchanged1)-] {+1:1.7.9-3~jan+lint1)+}
* Installed-Size: [-515-] {+204+}
* Version: [-1:1.7.9-2~jan+unchanged1-] {+1:1.7.9-3~jan+lint1+}

You can also view the [diffoscope diff](https://www.example.com/api/run/some-log-id/diffoscope?filter_boring=1) ([unfiltered](https://www.example.com/api/run/some-log-id/diffoscope)).
""" == env.get_template("binary-diff.md").render(
        {"role": "main", "log_id": "some-log-id", "debdiff": DEBDIFF}
    )


def test_render_mia():
    assert """\
Remove MIA uploaders:

* [Joe Example](https://qa.debian.org/developer.php?login=joe@example.com)
* [Jane Example](https://qa.debian.org/developer.php?login=jane@example.com)

See bugs [12345](https://bugs.debian.org/12345) for details.

This merge proposal was created by the [Janitor bot](https://www.example.com/mia), and it will automatically rebase or close this proposal as appropriate when the target branch changes. Any comments you leave here will be read by the Janitor's maintainers.

Build and test logs for this branch can be found at https://www.example.com/run/some-log-id.\
""" == env.get_template("mia.md").render(
        {
            "package": "blah",
            "log_id": "some-log-id",
            "campaign": "mia",
            "suite": "mia",
            "role": "main",
            "removed_uploaders": [
                "Joe Example <joe@example.com>",
                "Jane Example <jane@example.com>",
            ],
            "bugs": [12345],
        }
    )


def test_render_lintian_fixes():
    assert (
        env.get_template("lintian-fixes.md").render(
            {
                "package": "blah",
                "log_id": "some-log-id",
                "campaign": "lintian-fixes",
                "suite": "lintian-fixes",
                "role": "main",
                "applied": [
                    {
                        "summary": "Re-export upstream signing key without extra signatures.",
                        "description": "Re-export upstream signing key without extra signatures.",
                        "fixed_lintian_tags": ["public-upstream-key-not-minimal"],
                        "revision_id": "git-v1:213654ea0b502f4a2b033a6e7ea90835a0640cf1",
                        "certainty": None,
                    },
                    {
                        "summary": "Set upstream metadata fields: Bug-Database, Bug-Submit.",
                        "description": "Set upstream metadata fields: Bug-Database, Bug-Submit.",
                        "fixed_lintian_tags": [
                            "upstream-metadata-missing-bug-tracking"
                        ],
                        "revision_id": "git-v1:c9c2fa3bc18028fa8e12f960452811c7b42c47fe",
                        "certainty": "certain",
                    },
                ],
                "failed": {},
            }
        )
        == """\
Fix some issues reported by lintian

* Re-export upstream signing key without extra signatures. ([public-upstream-key-not-minimal](https://lintian.debian.org/tags/public-upstream-key-not-minimal))
* Set upstream metadata fields: Bug-Database, Bug-Submit. ([upstream-metadata-missing-bug-tracking](https://lintian.debian.org/tags/upstream-metadata-missing-bug-tracking))

This merge proposal was created by the [Janitor bot](https://www.example.com/lintian-fixes), and it will automatically rebase or close this proposal as appropriate when the target branch changes. Any comments you leave here will be read by the Janitor's maintainers.

Build and test logs for this branch can be found at https://www.example.com/run/some-log-id.\
"""
    )

    assert (
        env.get_template("lintian-fixes.md").render(
            {
                "package": "blah",
                "log_id": "some-log-id",
                "campaign": "lintian-fixes",
                "suite": "lintian-fixes",
                "role": "main",
                "debdiff": "FOO\n" * 50,
                "applied": [
                    {
                        "summary": "Re-export upstream signing key without extra signatures.",
                        "description": "Re-export upstream signing key without extra signatures.",
                        "fixed_lintian_tags": ["public-upstream-key-not-minimal"],
                        "revision_id": "git-v1:213654ea0b502f4a2b033a6e7ea90835a0640cf1",
                        "certainty": None,
                    },
                    {
                        "summary": "Set upstream metadata fields: Bug-Database, Bug-Submit.",
                        "description": "Set upstream metadata fields: Bug-Database, Bug-Submit.",
                        "fixed_lintian_tags": [
                            "upstream-metadata-missing-bug-tracking"
                        ],
                        "revision_id": "git-v1:c9c2fa3bc18028fa8e12f960452811c7b42c47fe",
                        "certainty": "certain",
                    },
                ],
                "failed": {},
            }
        )
        == """\
Fix some issues reported by lintian

* Re-export upstream signing key without extra signatures. ([public-upstream-key-not-minimal](https://lintian.debian.org/tags/public-upstream-key-not-minimal))
* Set upstream metadata fields: Bug-Database, Bug-Submit. ([upstream-metadata-missing-bug-tracking](https://lintian.debian.org/tags/upstream-metadata-missing-bug-tracking))

These changes have no impact on the [binary debdiff](https://www.example.com/api/run/some-log-id/debdiff?filter_boring=1).

You can also view the [diffoscope diff](https://www.example.com/api/run/some-log-id/diffoscope?filter_boring=1) ([unfiltered](https://www.example.com/api/run/some-log-id/diffoscope)).

This merge proposal was created by the [Janitor bot](https://www.example.com/lintian-fixes), and it will automatically rebase or close this proposal as appropriate when the target branch changes. Any comments you leave here will be read by the Janitor's maintainers.

Build and test logs for this branch can be found at https://www.example.com/run/some-log-id.\
"""
    )


def test_render_orphan():
    assert (
        env.get_template("orphan.md").render(
            {
                "package": "blah",
                "log_id": "some-log-id",
                "campaign": "orphan",
                "suite": "orphan",
                "role": "main",
                "old_vcs_url": None,
                "new_vcs_url": None,
                "pushed": False,
                "salsa_user": None,
                "wnpp_bug": 995502,
            }
        )
        == """\
Move orphaned package to the QA team.

For details, see the [orphan bug](https://bugs.debian.org/995502).

This merge proposal was created by the [Janitor bot](https://www.example.com/orphan), and it will automatically rebase or close this proposal as appropriate when the target branch changes. Any comments you leave here will be read by the Janitor's maintainers.

Build and test logs for this branch can be found at https://www.example.com/run/some-log-id.\
"""
    )


def test_render_multiarch_fixes():
    assert (
        env.get_template("multiarch-fixes.md").render(
            {
                "package": "blah",
                "log_id": "some-log-id",
                "campaign": "multiarch-fixes",
                "suite": "multiarch-fixes",
                "role": "main",
                "codemod": {
                    "applied-hints": [
                        {
                            "binary": "inventor-doc",
                            "description": "inventor-doc could be marked Multi-Arch: foreign",
                            "link": "https://wiki.debian.org/MultiArch/Hints#ma-foreign",
                            "severity": "low",
                            "source": "inventor",
                            "version": "2.1.5-10-25",
                            "action": "Add Multi-Arch: foreign.",
                            "certainty": "certain",
                        },
                        {
                            "binary": "inventor-data",
                            "description": "inventor-data could be marked Multi-Arch: foreign",
                            "link": "https://wiki.debian.org/MultiArch/Hints#ma-foreign",
                            "severity": "low",
                            "source": "inventor",
                            "version": "2.1.5-10-25",
                            "action": "Add Multi-Arch: foreign.",
                            "certainty": "certain",
                        },
                    ]
                },
            }
        )
        == """\
Apply hints suggested by the [multi-arch hinter](https://wiki.debian.org/MultiArch/Hints).

* inventor-doc: Add Multi-Arch: foreign. This fixes: inventor-doc could be marked Multi-Arch: foreign. ([ma-foreign](https://wiki.debian.org/MultiArch/Hints#ma-foreign))
* inventor-data: Add Multi-Arch: foreign. This fixes: inventor-data could be marked Multi-Arch: foreign. ([ma-foreign](https://wiki.debian.org/MultiArch/Hints#ma-foreign))

Note that in some cases, these multi-arch hints may trigger lintian warnings until the dependencies of the package support multi-arch. This is expected, see [the FAQ](https://janitor.debian.net/multiarch-fixes#why-does-lintian-warn) for details.

This merge proposal was created by the [Janitor bot](https://www.example.com/multiarch-fixes), and it will automatically rebase or close this proposal as appropriate when the target branch changes. Any comments you leave here will be read by the Janitor's maintainers.

Build and test logs for this branch can be found at https://www.example.com/run/some-log-id.\
"""
    )


def test_render_scrub_obsolete():
    assert (
        env.get_template("scrub-obsolete.md").render(
            {
                "package": "blah",
                "log_id": "some-log-id",
                "campaign": "scrub-obsolete",
                "suite": "scrub-obsolete",
                "role": "main",
            }
        )
        == """\
Remove unnecessary constraints.

This merge proposal was created by the [Janitor bot](https://www.example.com/scrub-obsolete), and it will automatically rebase or close this proposal as appropriate when the target branch changes. Any comments you leave here will be read by the Janitor's maintainers.

Build and test logs for this branch can be found at https://www.example.com/run/some-log-id.\
"""
    )
