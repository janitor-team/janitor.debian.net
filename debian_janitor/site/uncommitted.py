#!/usr/bin/python3

from janitor.site.common import html_template

CAMPAIGN = "uncommitted"


@html_template("uncommitted/start.html")
async def handle_uncommitted_start(request):
    return {}


def register_uncommitted_endpoints(router):
    router.add_get("/uncommitted/", handle_uncommitted_start, name="uncommitted-start")
