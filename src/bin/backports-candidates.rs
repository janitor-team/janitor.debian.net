use clap::Parser;

use debian_janitor::candidates::Candidate;
use debversion::Version;

use distro_info::DistroInfo;

use sqlx::Arguments;
use std::error::Error;

fn debian_testing() -> String {
    let di = distro_info::DebianDistroInfo::new().unwrap();
    di.iter()
        .find(|d| d.release().is_none() && d.series() != "sid" && d.series() != "experimental")
        .unwrap()
        .series()
        .to_string()
}

fn debian_stable() -> String {
    let di = distro_info::DebianDistroInfo::new().unwrap();
    di.iter()
        .filter(|d| d.release().is_some())
        .last()
        .unwrap()
        .series()
        .to_string()
}

#[derive(Parser, Debug)]
struct Args {
    #[arg(short, long, value_name = "FILE")]
    config: Option<std::path::PathBuf>,

    #[command(flatten)]
    udd: debian_janitor::udd::UDDArgs,

    #[command(flatten)]
    logging: janitor::logging::LoggingArgs,

    /// Source release to backport from
    #[arg(short, long, value_name = "RELEASE", default_value_t = debian_testing())]
    source_release: String,

    /// Target release to backport to
    #[arg(short, long, value_name = "RELEASE", default_value_t = debian_stable())]
    target_release: String,

    /// Specific packages to backport
    packages: Option<Vec<String>>,
}

const DEFAULT_VALUE_BACKPORTS: i32 = 50;
const EXISTING_BACKPORT_VALUE: i32 = 60;

#[derive(sqlx::FromRow, Debug)]
struct BackportRow {
    source: String,
    source_version: Option<Version>,
    target_version: Option<Version>,
    backport_version: Option<Version>,
}

pub async fn iter_backports_candidates(
    udd: &sqlx::Pool<sqlx::Postgres>,
    source_release: &str,
    target_release: &str,
    packages: Option<Vec<&str>>,
) -> Result<(), sqlx::Error> {
    let mut args = sqlx::postgres::PgArguments::default();
    args.add(source_release);
    args.add(target_release);

    let mut query = "\
        SELECT \
            source_series.source AS source, \
            source_series.version AS source_version, \
            target_series.version AS target_version, \
            backport_series.version AS backport_version \
        FROM sources AS source_series \
        LEFT JOIN sources AS target_series \
            ON source_series.source = target_series.source AND target_series.release = $2 \
        LEFT JOIN sources AS backport_series \
            ON source_series.source = backport_series.source \
            AND backport_series.release = CONCAT($2, '-backports') \
        WHERE \
            source_series.vcs_url != '' \
            AND source_series.release = $1 \
            AND (target_series.version IS NULL OR source_series.version > target_series.version) \
            AND (backport_series.version IS NULL \
                OR source_series.version > backport_series.version)"
        .to_string();

    if let Some(packages) = packages {
        query.push_str(" AND source_series.source = ANY($3::text[])");
        args.add(packages);
    }

    let rows: Vec<BackportRow> = sqlx::query_as_with(&query, args).fetch_all(udd).await?;

    for row in rows {
        let source_uversion = &row.source_version.as_ref().unwrap().upstream_version;
        let current_uversion = if let Some(backport_version) = row.backport_version.as_ref() {
            Some(&backport_version.upstream_version)
        } else {
            row.target_version
                .as_ref()
                .map(|target_version| &target_version.upstream_version)
        };

        if let Some(current_uversion) = current_uversion {
            if source_uversion == current_uversion {
                continue;
            }
        }

        println!(
            "# target currently has {}",
            row.target_version
                .map_or_else(|| "none".to_string(), |v| v.to_string())
        );
        println!(
            "# backports currently has {}",
            row.backport_version
                .as_ref()
                .map_or_else(|| "none".to_string(), |v| v.to_string())
        );

        let mut value = DEFAULT_VALUE_BACKPORTS;
        if row.backport_version.is_some() {
            value += EXISTING_BACKPORT_VALUE;
        }

        let candidate = Candidate {
            distribution: Some("sid".to_string()),
            package: Some(row.source.clone()),
            value: Some(value),
            campaign: Some(format!("{}-backports", target_release)),
            context: row.source_version.map(|v| v.to_string()),
            extra_arg: vec![format!("--source={}", row.source)],
            ..Default::default()
        };

        debian_janitor::candidates::print_candidate_as_textpb(candidate);
    }

    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let args = Args::parse();

    args.logging.init();

    let pool = args.udd.connect().await?;

    let mut tags = std::collections::HashSet::new();
    for fixer in lintian_brush::available_lintian_fixers(None, None)? {
        tags.extend(fixer.lintian_tags().into_iter());
    }

    iter_backports_candidates(
        &pool,
        args.source_release.as_str(),
        args.target_release.as_str(),
        args.packages
            .as_ref()
            .map(|ps| ps.iter().map(|s| s.as_str()).collect()),
    )
    .await?;

    Ok(())
}
