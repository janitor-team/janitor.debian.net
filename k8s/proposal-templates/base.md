{# Maximum number of lines of debdiff to inline in the merge request
   description. If this threshold is reached, we'll just include a link to thj
   debdiff.
-#}
{% block codemod %}{% endblock %}

This merge proposal was created by the [Janitor bot]({{ external_url }}/{{ campaign }}), and it will automatically rebase or close this proposal as appropriate when the target branch changes. Any comments you leave here will be read by the Janitor's maintainers.

Build and test logs for this branch can be found at {{ external_url }}/run/{{ log_id }}.
