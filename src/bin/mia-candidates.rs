use clap::Parser;

use sqlx::postgres::{PgPool, PgPoolOptions};
use sqlx::Arguments;
use std::error::Error;

const DEFAULT_VALUE_MIA: i32 = 70;
const MIA_EMAIL: &str = "mia@qa.debian.org";
const MIA_TEAMMAINT_USERTAG: &str = "mia-teammaint";

#[derive(Parser, Debug)]
struct Args {
    /// Enable debug mode.
    #[arg(long, default_value_t = false)]
    debug: bool,

    /// Use Google cloud logging.
    #[arg(long, default_value_t = false)]
    gcp_logging: bool,

    #[arg(short, long, value_name = "FILE")]
    config: Option<std::path::PathBuf>,

    #[arg(long, value_name = "URL", default_value_t = debian_janitor::udd::default_udd_url())]
    udd_url: String,

    packages: Option<Vec<String>>,
}

#[derive(sqlx::FromRow)]
struct BugRow {
    source: String,
    id: i32,
}

async fn iter_mia_candidates(
    pool: &PgPool,
    release: &str,
    packages: Option<Vec<&str>>,
) -> Result<(), Box<dyn Error>> {
    let mut query = "SELECT source, id FROM bugs \
        WHERE id IN (SELECT id FROM bugs_usertags WHERE email = $1 AND tag = $2) \
        AND status = 'pending'"
        .to_string();

    let mut args = sqlx::postgres::PgArguments::default();
    args.add(MIA_EMAIL);
    args.add(MIA_TEAMMAINT_USERTAG);

    if let Some(packages) = packages {
        query.push_str(" AND sources.source = ANY($3)");
        args.add(packages);
    }

    let rows: Vec<BugRow> = sqlx::query_as_with(&query, args).fetch_all(pool).await?;

    for row in rows {
        let candidate = debian_janitor::candidates::Candidate {
            distribution: Some(release.to_string()),
            package: Some(row.source),
            campaign: Some("mia".to_string()),
            context: Some(row.id.to_string()),
            value: Some(DEFAULT_VALUE_MIA),
            ..Default::default()
        };

        debian_janitor::candidates::print_candidate_as_textpb(candidate);
    }

    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let args = Args::parse();

    // TODO(jelmer): Add common function for initializing logging
    if args.gcp_logging {
        stackdriver_logger::init_with_cargo!("../../Cargo.toml");
    } else if args.debug {
        env_logger::init();
    } else {
        env_logger::builder()
            .filter(None, log::LevelFilter::Info)
            .init();
    }

    // TODO(jelmer): Add common function for connecting to udd
    let pool = PgPoolOptions::new()
        .max_connections(5)
        .connect(args.udd_url.as_str())
        .await?;

    iter_mia_candidates(
        &pool,
        "sid",
        args.packages
            .as_ref()
            .map(|ps| ps.iter().map(|s| s.as_str()).collect()),
    )
    .await?;

    Ok(())
}
