use clap::Parser;

use debian_janitor::candidates::Candidate;
use debversion::Version;

use sqlx::postgres::PgPool;
use sqlx::Arguments;
use std::error::Error;

#[derive(Parser, Debug)]
struct Args {
    #[arg(short, long, value_name = "FILE")]
    config: Option<std::path::PathBuf>,

    #[command(flatten)]
    udd: debian_janitor::udd::UDDArgs,

    #[command(flatten)]
    logging: janitor::logging::LoggingArgs,

    packages: Option<Vec<String>>,
}

#[derive(sqlx::FromRow, Debug)]
struct SourceRow {
    source: String,
    version: Version,
    vcs_type: Option<String>,
    vcs_url: Option<String>,
    maintainer_email: Option<String>,
    uploaders: Option<String>,
    tags: Vec<String>,
}

async fn iter_lintian_fixes_candidates(
    pool: &PgPool,
    release: &str,
    packages: Option<Vec<&str>>,
    available_fixers: std::collections::HashSet<&str>,
) -> Result<(), Box<dyn std::error::Error>> {
    let mut package_rows = std::collections::HashMap::new();
    let mut package_tags = std::collections::HashMap::new();

    let mut args = sqlx::postgres::PgArguments::default();
    args.add(&available_fixers.clone().into_iter().collect::<Vec<_>>());
    args.add(&release);

    let mut query = "
        SELECT DISTINCT ON (sources.source)
        sources.source,
        sources.version,
        sources.vcs_type,
        sources.vcs_url,
        sources.maintainer_email,
        sources.uploaders,
        ARRAY(SELECT tag FROM lintian WHERE
            sources.source = lintian.package AND
            sources.version = lintian.package_version AND
            lintian.package_type = 'source' AND
            tag = any($1::text[])
        ) AS tags
        FROM sources
        WHERE
        sources.release = $2
        AND vcs_type != ''
    "
    .to_string();

    if let Some(packages) = packages.as_ref() {
        query += " AND sources.source = any($3::text[])";
        args.add(packages);
    }

    query += " ORDER BY sources.source, sources.version DESC";

    let rows: Vec<SourceRow> = sqlx::query_as_with(&query, args).fetch_all(pool).await?;

    for row in rows {
        package_rows.insert(
            row.source.clone(),
            (
                row.version,
                row.vcs_type,
                row.vcs_url,
                row.maintainer_email,
                row.uploaders,
                row.tags.clone(),
            ),
        );
        package_tags.insert(row.source, row.tags);
    }

    let mut args = sqlx::postgres::PgArguments::default();
    args.add(&available_fixers.into_iter().collect::<Vec<_>>());
    args.add(&release);

    let mut query = "
        SELECT DISTINCT ON (sources.source)
        sources.source,
        sources.version,
        sources.vcs_type,
        sources.vcs_url,
        sources.maintainer_email,
        sources.uploaders,
        ARRAY(SELECT lintian.tag FROM lintian
            INNER JOIN packages ON packages.package = lintian.package
            AND packages.version = lintian.package_version
        WHERE
            lintian.tag = any($1::text[]) AND
            lintian.package_type = 'binary' AND
            sources.version = packages.version AND
            sources.source = packages.source
        ) AS tags
        FROM sources
        WHERE sources.release = $2 AND vcs_type != ''
    "
    .to_string();

    if let Some(packages) = packages.as_ref() {
        query += " AND sources.source = any($3::text[])";
        args.add(packages);
    }

    query += " ORDER BY sources.source, sources.version DESC";

    let rows: Vec<SourceRow> = sqlx::query_as_with(&query, args).fetch_all(pool).await?;

    for row in rows {
        package_tags
            .entry(row.source)
            .or_insert_with(Vec::new)
            .extend(row.tags)
    }

    for (name, _row) in package_rows {
        let tags = package_tags
            .get(&name)
            .cloned().unwrap_or_default();
        let value = lintian_brush::calculate_value(
            tags.iter()
                .map(|x| x.as_str())
                .collect::<Vec<_>>()
                .as_slice(),
        );
        let context = tags.to_vec().join(" ");
        let candidate = Candidate {
            distribution: Some(release.to_string()),
            package: Some(name),
            context: Some(context),
            value: Some(value),
            campaign: Some("lintian-fixes".to_string()),
            ..Default::default()
        };

        debian_janitor::candidates::print_candidate_as_textpb(candidate);
    }

    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let args = Args::parse();

    args.logging.init();

    let pool = args.udd.connect().await?;

    let mut tags = std::collections::HashSet::new();
    for fixer in lintian_brush::available_lintian_fixers(None, None)? {
        tags.extend(fixer.lintian_tags().into_iter());
    }

    iter_lintian_fixes_candidates(
        &pool,
        "sid",
        args.packages
            .as_ref()
            .map(|ps| ps.iter().map(|s| s.as_str()).collect()),
        tags.iter().map(|s| s.as_str()).collect(),
    )
    .await?;

    Ok(())
}
