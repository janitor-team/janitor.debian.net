#!/usr/bin/python3

from setuptools import setup
from setuptools_protobuf import Protobuf
from setuptools_rust import Binding, RustExtension, RustBin

setup(
    protobufs=[
        Protobuf("debian_janitor/policy.proto", mypy=True),
        Protobuf("debian_janitor/candidates.proto", mypy=True),
        Protobuf("debian_janitor/package_metadata.proto", mypy=True),
        Protobuf("debian_janitor/package_overrides.proto", mypy=True),
        Protobuf("debian_janitor/upstream_project.proto", mypy=True),
    ],
    rust_extensions=[
        RustBin("analyze-branch-unknown", "Cargo.toml"),
        RustBin("refresh-policy", "Cargo.toml"),
        RustBin("import-repology", "Cargo.toml"),
        RustBin("mia-candidates", "Cargo.toml"),
        RustBin("orphan-candidates", "Cargo.toml"),
        RustBin("vcswatch-candidates", "Cargo.toml"),
        RustBin("fresh-releases-candidates", "Cargo.toml"),
        RustBin("fresh-snapshots-candidates", "Cargo.toml"),
        RustBin("watch-candidates", "Cargo.toml"),
        RustBin("scrub-obsolete-candidates", "Cargo.toml"),
        RustBin("test-debianize-candidates", "Cargo.toml"),
        RustBin("backports-candidates", "Cargo.toml"),
        RustBin("unchanged-candidates", "Cargo.toml"),
        RustBin("multi-arch-candidates", "Cargo.toml"),
        RustBin("candidates", "Cargo.toml"),
        RustExtension(
            "debian_janitor._debian_janitor_rs",
            "debian-janitor-py/Cargo.toml",
            binding=Binding.PyO3,
        ),
    ],
)
