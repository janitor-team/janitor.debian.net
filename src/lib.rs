mod generated {
    include!(concat!(env!("OUT_DIR"), "/generated/mod.rs"));
}

pub mod candidates;
pub mod overrides;
pub mod package_metadata;
pub mod packages;
pub mod policy;
pub mod release;
pub mod repology;
pub mod udd;
pub mod vcs_url;

const ALIOTH_HOSTS: &[&str] = &[
    "svn.debian.org",
    "bzr.debian.org",
    "anonscm.debian.org",
    "hg.debian.org",
    "git.debian.org",
    "alioth.debian.org",
];

pub fn is_alioth_url(url: &url::Url) -> bool {
    ALIOTH_HOSTS.contains(&url.host_str().unwrap_or(""))
}
