pub fn default_udd_url() -> String {
    std::env::var("UDD_URL").ok().unwrap_or_else(|| {
        "postgresql://udd-mirror:udd-mirror@udd-mirror.debian.net:5432/udd".to_string()
    })
}

#[derive(clap::Args, Debug, Clone)]
#[group()]
pub struct UDDArgs {
    /// Postgres URL of the UDD database
    #[arg(long, value_name = "URL", default_value_t = default_udd_url())]
    udd_url: String,
}

impl UDDArgs {
    pub fn udd_url(&self) -> &str {
        &self.udd_url
    }

    pub async fn connect(&self) -> Result<sqlx::PgPool, sqlx::Error> {
        sqlx::postgres::PgPoolOptions::new()
            .max_connections(5)
            .connect(self.udd_url.as_str())
            .await
    }
}
